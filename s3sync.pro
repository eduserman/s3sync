#-------------------------------------------------
#
# Project created by QtCreator 2013-06-07T10:57:27
#
#-------------------------------------------------

QT       += core gui sql concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = s3sync
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    paireditiondialog.cpp \
    allpairssubform.cpp \
    pairslistform.cpp \
    pairsubform.cpp \
    model/version.cpp \
    model/pair.cpp \
    model/item.cpp \
    algorithm/icommand.cpp \
    data/sqlite3user.cpp \
    data/sqlite3connectorfactory.cpp \
    data/sqlite3connector.cpp \
    data/iuser.cpp \
    data/iconnectorfactory.cpp \
    data/iconnector.cpp \
    data/connectorfactoryinstaller.cpp \
    error/exception.cpp \
    connectorsingleton.cpp \
    algorithm/unidirectionalcompare.cpp \
    algorithm/bidirectionalcompare.cpp \
    algorithm/deletefilecommand.cpp \
    algorithm/copyfilecommand.cpp \
    algorithm/copydircommand.cpp \
    algorithm/deletedircommand.cpp \
    synchronizationpreviewform.cpp \
    synchronizationrunform.cpp \
    algorithm/icomparecommand.cpp \
    algorithm/ifilesystemcommand.cpp \
    algorithm/overwritefilecommand.cpp \
    algorithm/batchcommand.cpp \
    algorithm/updatelastmodificationcommand.cpp \
    aboutdialog.cpp

HEADERS  += mainwindow.h \
    paireditiondialog.h \
    allpairssubform.h \
    pairslistform.h \
    pairsubform.h \
    model/version.h \
    model/pair.h \
    model/item.h \
    algorithm/icommand.h \
    data/sqlite3user.h \
    data/sqlite3connectorfactory.h \
    data/sqlite3connector.h \
    data/iuser.h \
    data/iconnectorfactory.h \
    data/iconnector.h \
    data/connectorfactoryinstaller.h \
    error/exception.h \
    connectorsingleton.h \
    algorithm/unidirectionalcompare.h \
    algorithm/bidirectionalcompare.h \
    algorithm/deletefilecommand.h \
    algorithm/copyfilecommand.h \
    algorithm/copydircommand.h \
    algorithm/deletedircommand.h \
    synchronizationpreviewform.h \
    synchronizationrunform.h \
    algorithm/icomparecommand.h \
    algorithm/ifilesystemcommand.h \
    algorithm/overwritefilecommand.h \
    algorithm/batchcommand.h \
    algorithm/updatelastmodificationcommand.h \
    aboutdialog.h

FORMS    += mainwindow.ui \
    paireditiondialog.ui \
    allpairssubform.ui \
    pairslistform.ui \
    pairsubform.ui \
    synchronizationpreviewform.ui \
    synchronizationrunform.ui \
    aboutdialog.ui

OTHER_FILES += \
    images/Open-Folder-Warning-icon.png \
    images/Open-Folder-Info-icon.png \
    images/Open-Folder-Full-icon.png \
    images/Open-Folder-Delete-icon.png \
    images/Open-Folder-Add-icon.png \
    images/Open-Folder-Accept-icon.png

RESOURCES += \
    s3sync.qrc
