#include "item.h"

namespace S3sync {
namespace Model {

Item::Item(QObject *parent) :
    QObject(parent),
    id(0),
    pairId(0)
{
}

Item::Item(const Item& other) :
    QObject(other.parent()),
    id(other.getId()),
    pairId(other.getPairId()),
    sourcePath(other.getSourcePath()),
    sourceModificationDate(other.getSourceModificationDate()),
    targetPath(other.getTargetPath()),
    targetModificationDate(other.getTargetModificationDate())
{

}

Item& Item::operator=(const Item& other)
{
    setParent(other.parent());
    id = other.getId();
    pairId = other.getPairId();
    sourcePath = other.getSourcePath();
    sourceModificationDate = other.getSourceModificationDate();
    targetPath = other.getTargetPath();
    targetModificationDate = other.getTargetModificationDate();
    return *this;
}

bool Item::operator==(const Item& other) const
{
    return id == other.getId();
}

bool Item::operator!=(const Item& other) const
{
    return id != other.getId();
}

}
}
