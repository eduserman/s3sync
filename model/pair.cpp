#include "pair.h"

namespace S3sync {
namespace Model {

Pair::Pair(QObject *parent) :
    QObject(parent),
    id(0),
    name(),
    sourcePath(),
    targetPath(),
    syncType(ST_BIDIRECTIONAL),
    active(true),
    excludeReadOnly(false),
    excludeHidden(false),
    excludeSystem(false),
    inclusionFilter("*"),
    exclusionFilter()
{
}

Pair::Pair(const Pair& other) :
    QObject(other.parent()),
    id(other.getId()),
    name(other.getName()),
    sourcePath(other.getSourcePath()),
    targetPath(other.getTargetPath()),
    syncType(other.getSynchronizationType()),
    active(other.isActive()),
    excludeReadOnly(other.excludeReadOnly),
    excludeHidden(other.excludeHidden),
    excludeSystem(other.excludeSystem),
    inclusionFilter(other.inclusionFilter),
    exclusionFilter(other.exclusionFilter)
{

}

Pair& Pair::operator=(const Pair& other)
{
    setParent(other.parent());
    id = other.getId();
    name = other.getName();
    sourcePath = other.getSourcePath();
    targetPath = other.getTargetPath();
    syncType = other.getSynchronizationType();
    active = other.isActive();
    excludeReadOnly = other.excludeReadOnly;
    excludeHidden = other.excludeHidden;
    excludeSystem = other.excludeSystem;
    inclusionFilter = other.inclusionFilter;
    exclusionFilter = other.exclusionFilter;
    return *this;
}

bool Pair::operator==(const Pair& other) const
{
    return id == other.getId();
}

bool Pair::operator!=(const Pair& other) const
{
    return id != other.getId();
}

}
}
