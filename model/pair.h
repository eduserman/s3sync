#ifndef PAIR_H
#define PAIR_H

#include <QObject>
#include <QMap>

namespace S3sync {
namespace Model {

/**
 * @brief The Pair class represents a pair of folders being synchronized
 */
class Pair : public QObject
{
    Q_OBJECT

public:

    /// The typed used as pair key
    typedef qint32 Key;

    /// The type used for storing collection of pairs
    typedef QMap<Key,Pair> Collection;

    /**
     * @brief Creates a new Pair
     * @param parent The reference to the owner QObject
     */
    explicit Pair(QObject *parent = 0);

    /**
     * @brief Creates a new Pair
     * @param other The Pair instance that will be used as model for this one creation
     */
    Pair(const Pair& other);

    /**
      * Assignment operator.
      * @param other the Pair to be copied
      * @return the changed Pair instance
      */
    Pair& operator=(const Pair& other);

    /**
      * Comparison operator.
      * @param other the Pair to be compared
      * @return true if both pairs are identical
      */
    bool operator==(const Pair& other) const;

    /**
      * Comparison operator.
      * @param other the Pair to be compared
      * @return true if both pairs are different
      */
    bool operator!=(const Pair& other) const;

signals:
    
public slots:

public:

    /// Possible synchronization types
    typedef enum _SYNC_TYPE{

        /// Unidirectional synchronization type
        ST_UNIDIRECTIONAL,

        /// Bidirectional synchronization type
        ST_BIDIRECTIONAL

    } SYNC_TYPE;

    /**
     * @brief Identification number read accessor
     * @return the identification number
     */
    inline qint32 getId() const;

    /**
     * @brief Identification number write accessor
     * @param value the new identification number
     */
    inline void setId(qint32 value);

    /**
     * @brief Name read accessor
     * @return the name
     */
    inline const QString& getName() const;

    /**
     * @brief Name write accessor
     * @param value the new name
     */
    inline void setName(const QString& value);

    /**
     * @brief Source path read accessor
     * @return the source path
     */
    inline const QString& getSourcePath() const;

    /**
     * @brief Source path write accessor
     * @param value the new source path
     */
    inline void setSourcePath(const QString& value);

    /**
     * @brief Target path read accessor
     * @return the target path
     */
    inline const QString& getTargetPath() const;

    /**
     * @brief Target path write accessor
     * @param value the new target path
     */
    inline void setTargetPath(const QString& value);

    /**
     * @brief Synchronization type read accessor
     * @return the synchronization type
     */
    inline SYNC_TYPE getSynchronizationType() const;

    /**
     * @brief Synchronization type write accessor
     * @param value the new synchronization type
     */
    inline void setSynchronizationType(SYNC_TYPE value);

    /**
     * @brief Activation state read accessor
     * @return the activation state
     */
    inline bool isActive() const;

    /**
     * @brief Activation state write accessor
     * @param value the new activation state
     */
    inline void setActive(bool value);

    /**
     * @brief ExcludeReadOnlyFiles flag read accessor
     * @return the flag state
     */
    inline bool excludeReadOnlyFiles() const;

    /**
     * @brief ExcludeReadOnlyFiles flag write accessor
     * @param value the new flag state
     */
    inline void setExcludeReadOnlyFiles(bool value);

    /**
     * @brief ExcludeHiddenFiles flag read accessor
     * @return the flag state
     */
    inline bool excludeHiddenFiles() const;

    /**
     * @brief ExcludeHiddenFiles flag write accessor
     * @param value the new flag state
     */
    inline void setExcludeHiddenFiles(bool value);

    /**
     * @brief ExcludeSystemFiles flag read accessor
     * @return the flag state
     */
    inline bool excludeSystemFiles() const;

    /**
     * @brief ExcludeSystemFiles flag write accessor
     * @param value the new flag state
     */
    inline void setExcludeSystemFiles(bool value);

    /**
     * @brief InclusionFilter read accessor
     * @return the inclusion filter
     */
    inline const QString& getInclusionFilter() const;

    /**
     * @brief InclusionFilter write accessor
     * @param value the new inclusion filter
     */
    inline void setInclusionFilter(const QString& value);

    /**
     * @brief ExclusionFilter read accessor
     * @return the exclusion filter
     */
    inline const QString& getExclusionFilter() const;

    /**
     * @brief ExclusionFilter write accessor
     * @param value the new exclusion filter
     */
    inline void setExclusionFilter(const QString& value);

private:

    /// Pair identification number
    Key id;

    /// Pair name
    QString name;

    /// Source path
    QString sourcePath;

    /// Target path
    QString targetPath;

    /// Synchronization type
    SYNC_TYPE syncType;

    /// Activation state
    bool active;

    /// Exclude read only files
    bool excludeReadOnly;

    /// Exclude hidden files
    bool excludeHidden;

    /// Exclude system files
    bool excludeSystem;

    /// Include filter
    QString inclusionFilter;

    /// Exclude filter
    QString exclusionFilter;
};

inline qint32 Pair::getId() const
{
    return id;
}

inline void Pair::setId(qint32 value)
{
    id = value;
}

inline const QString& Pair::getName() const
{
    return name;
}

inline void Pair::setName(const QString& value)
{
    name = value;
}

inline const QString& Pair::getSourcePath() const
{
    return sourcePath;
}

inline void Pair::setSourcePath(const QString& value)
{
    sourcePath = value;
}

inline const QString& Pair::getTargetPath() const
{
    return targetPath;
}

inline void Pair::setTargetPath(const QString& value)
{
    targetPath = value;
}

inline Pair::SYNC_TYPE Pair::getSynchronizationType() const
{
    return syncType;
}

inline void Pair::setSynchronizationType(Pair::SYNC_TYPE value)
{
    syncType = value;
}

inline bool Pair::isActive() const
{
    return active;
}

inline void Pair::setActive(bool value)
{
    active = value;
}

inline bool Pair::excludeReadOnlyFiles() const
{
    return excludeReadOnly;
}

inline void Pair::setExcludeReadOnlyFiles(bool value)
{
    excludeReadOnly = value;
}

inline bool Pair::excludeHiddenFiles() const
{
    return excludeHidden;
}

inline void Pair::setExcludeHiddenFiles(bool value)
{
    excludeHidden = value;
}

inline bool Pair::excludeSystemFiles() const
{
    return excludeSystem;
}

inline void Pair::setExcludeSystemFiles(bool value)
{
    excludeSystem = value;
}

inline const QString& Pair::getInclusionFilter() const
{
    return inclusionFilter;
}

inline void Pair::setInclusionFilter(const QString& value)
{
    inclusionFilter = value;
}

inline const QString& Pair::getExclusionFilter() const
{
    return exclusionFilter;
}

inline void Pair::setExclusionFilter(const QString& value)
{
    exclusionFilter = value;
}


}
}

#endif // PAIR_H
