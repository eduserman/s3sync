#ifndef VERSION_H
#define VERSION_H

#include <QObject>

namespace S3sync {
namespace Model {

/**
 * @brief The Version class is used to inform the database version.
 */
class Version : public QObject
{
    Q_OBJECT

public:

    /**
     * @brief Creates a new Version
     * @param parent The reference to the owner QObject
     */
    explicit Version(QObject *parent = 0);

    /**
     * @brief Creates a new Version
     * @param other The Version instance that will be used as model for this one creation
     */
    Version(const Version& other);

    /**
      * Assignment operator.
      * @param other the Version to be copied
      * @return the changed Version instance
      */
    Version& operator=(const Version& other);

    /**
      * Comparison operator.
      * @param other the Version to be compared
      * @return true if both Versions are identical
      */
    bool operator==(const Version& other) const;

    /**
      * Comparison operator.
      * @param other the Version to be compared
      * @return true if both Versions are different
      */
    bool operator!=(const Version& other) const;

    
signals:
    
public slots:

public:

    /**
     * @brief Major number read accessor
     * @return the major mumber
     */
    inline qint16 getMajorNumber() const;

    /**
     * @brief Major number write accessor
     * @param value the new major number
     */
    inline void setMajorNumber(qint16 value);

    /**
     * @brief Minor number read accessor
     * @return the minor number
     */
    inline qint16 getMinorNumber() const;

    /**
     * @brief Minor number write accessor
     * @param value the new minor number
     */
    inline void setMinorNumber(qint16 value);

    /**
     * @brief Description read accessor
     * @return the description
     */
    inline const QString& getDescription() const;

    /**
     * @brief Description write accessor
     * @param value the new description
     */
    inline void setDescription(const QString& value);
    
private:

    /// The major number
    qint16 majorNumber;

    /// The minor number
    qint16 minorNumber;

    /// The version optional description
    QString description;
};

inline qint16 Version::getMajorNumber() const
{
    return majorNumber;
}

inline void Version::setMajorNumber(qint16 value)
{
    majorNumber = value;
}

inline qint16 Version::getMinorNumber() const
{
    return minorNumber;
}

inline void Version::setMinorNumber(qint16 value)
{
    minorNumber = value;
}

inline const QString& Version::getDescription() const
{
    return description;
}

inline void Version::setDescription(const QString& value)
{
    description = value;
}

}
}

#endif // VERSION_H
