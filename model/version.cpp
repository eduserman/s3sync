#include "version.h"

namespace S3sync {
namespace Model {

Version::Version(QObject *parent) :
    QObject(parent),
    majorNumber(0),
    minorNumber(0)
{
}

Version::Version(const Version& other) :
    QObject(other.parent()),
    majorNumber(other.getMajorNumber()),
    minorNumber(other.getMinorNumber()),
    description(other.getDescription())
{

}

Version& Version::operator=(const Version& other)
{
    setParent(other.parent());
    majorNumber = other.getMajorNumber();
    minorNumber = other.getMinorNumber();
    description = other.getDescription();
    return *this;
}

bool Version::operator==(const Version& other) const
{
    return majorNumber == other.getMajorNumber() && minorNumber == other.getMinorNumber();
}

bool Version::operator!=(const Version& other) const
{
    return majorNumber != other.getMajorNumber() || minorNumber != other.getMinorNumber();
}

}
}
