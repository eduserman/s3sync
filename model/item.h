#ifndef ITEM_H
#define ITEM_H

#include <QObject>
#include <QDate>
#include <QMap>

namespace S3sync {
namespace Model {

/**
 * @brief The Item class represents an item that belons to a pair being synchronized.
 */
class Item : public QObject
{
    Q_OBJECT

public:

    /**
     * @brief Creates a new Item
     * @param parent The reference to the owner QObject
     */
    explicit Item(QObject *parent = 0);

    /**
     * @brief Creates a new Item
     * @param other The Item instance that will be used as model for this one creation
     */
    Item(const Item& other);

    /**
      * Assignment operator.
      * @param other the Item to be copied
      * @return the changed Item instance
      */
    Item& operator=(const Item& other);

    /**
      * Comparison operator.
      * @param other the Item to be compared
      * @return true if both Items are identical
      */
    bool operator==(const Item& other) const;

    /**
      * Comparison operator.
      * @param other the Item to be compared
      * @return true if both Items are different
      */
    bool operator!=(const Item& other) const;

signals:

public slots:

public:

    /**
     * @brief Identification number read accessor
     * @return the identification number
     */
    inline qint32 getId() const;

    /**
     * @brief Identification number write accessor
     * @param value the new identification number
     */
    inline void setId(qint32 value);

    /**
     * @brief Parent identification number read accessor
     * @return the parent identification number
     */
    inline qint32 getPairId() const;

    /**
     * @brief Parent identification number write accessor
     * @param value the new parent identification number
     */
    inline void setPairId(qint32 value);

    /**
     * @brief Source path read accessor
     * @return the source path
     */
    inline const QString& getSourcePath() const;

    /**
     * @brief Source path write accessor
     * @param value the new source path
     */
    inline void setSourcePath(const QString& value);

    /**
     * @brief Source modification date read accessor
     * @return the source modification date
     */
    inline const QDate& getSourceModificationDate() const;

    /**
     * @brief Source modification date write accessor
     * @param value the new source modification date
     */
    inline void setSourceModificationDate(const QDate& value);

    /**
     * @brief Target path read accessor
     * @return the target path
     */
    inline const QString& getTargetPath() const;

    /**
     * @brief Target path write accessor
     * @param value the new target path
     */
    inline void setTargetPath(const QString& value);

    /**
     * @brief Target modification date read accessor
     * @return the target modification date
     */
    inline const QDate& getTargetModificationDate() const;

    /**
     * @brief Target modification date write accessor
     * @param value the new target modification date.
     */
    inline void setTargetModificationDate(const QDate& value);

private:

    /// The typed used as item key
    typedef qint32 Key;

    /// The type used for storing collection of items
    typedef QMap<Key,Item> Collection;

    /// Item identification number
    qint32 id;

    /// The owner pair identification number
    qint32 pairId;

    /// Path to the source file
    QString sourcePath;

    /// Modification date of the source file
    QDate sourceModificationDate;

    /// Path to the target file
    QString targetPath;

    /// Modification date of the target file
    QDate targetModificationDate;
};

inline qint32 Item::getId() const
{
    return id;
}

inline void Item::setId(qint32 value)
{
    id = value;
}

inline qint32 Item::getPairId() const
{
    return pairId;
}

inline void Item::setPairId(qint32 value)
{
    pairId = value;
}

inline const QString& Item::getSourcePath() const
{
    return sourcePath;
}

inline void Item::setSourcePath(const QString& value)
{
    sourcePath = value;
}

inline const QDate& Item::getSourceModificationDate() const
{
    return sourceModificationDate;
}

inline void Item::setSourceModificationDate(const QDate& value)
{
    sourceModificationDate = value;
}

inline const QString& Item::getTargetPath() const
{
    return targetPath;
}

inline void Item::setTargetPath(const QString& value)
{
    targetPath = value;
}

inline const QDate& Item::getTargetModificationDate() const
{
    return targetModificationDate;
}

inline void Item::setTargetModificationDate(const QDate& value)
{
    targetModificationDate = value;
}

}
}

#endif // ITEM_H
