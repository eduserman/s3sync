/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "paireditiondialog.h"
#include "ui_paireditiondialog.h"

#include <QFileDialog>

PairEditionDialog::PairEditionDialog(const S3sync::Model::Pair& p, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PairEditionDialog),
    pair(p)
{
    // call the automatically generated ui configuration method
    ui->setupUi(this);

    // update the ui according to the pair contents
    updateControls();
}

PairEditionDialog::~PairEditionDialog()
{
    delete ui;
}

void PairEditionDialog::on_pushButtonSourceFolder_clicked()
{
    // select a directory using file dialog
    QString path = QFileDialog::getExistingDirectory(
                this,
                tr("Source Folder"),
                ui->lineEditSourceFolder->text());

    if (!path.isNull())
        ui->lineEditSourceFolder->setText(path);
}

void PairEditionDialog::on_pushButtonTargetFolder_clicked()
{
    // select a directory using file dialog
    QString path = QFileDialog::getExistingDirectory(
                this,
                tr("Target Folder"),
                ui->lineEditTargetFolder->text());

    if (!path.isNull())
        ui->lineEditTargetFolder->setText(path);
}

void PairEditionDialog::accept()
{
    // read the ui information
    QString name                        = this->ui->lineEditName->text();
    QString sourcePath                  = this->ui->lineEditSourceFolder->text();
    QString targetPath                  = this->ui->lineEditTargetFolder->text();
    bool    active                      = (this->ui->comboBoxActive->currentIndex() == 0);
    S3sync::Model::Pair::SYNC_TYPE type = (this->ui->comboBoxSynchType->currentIndex() == 0 ?
                                               S3sync::Model::Pair::ST_UNIDIRECTIONAL:
                                               S3sync::Model::Pair::ST_BIDIRECTIONAL );
    bool    excludeReadonly             = this->ui->checkBoxReadonly->isChecked();
    bool    excludeHidden               = this->ui->checkBoxHidden->isChecked();
    bool    excludeSystem               = this->ui->checkBoxSystem->isChecked();
    QString inclusionFilter             = this->ui->lineEditInclusionFilter->text();
    QString exclusionFilter             = this->ui->lineEditExclusionFilter->text();


    /// TODO: Add validations

    // if everythis is ok, configures the pair information accordingly
    pair.setName(name);
    pair.setSourcePath(sourcePath);
    pair.setTargetPath(targetPath);
    pair.setActive(active);
    pair.setSynchronizationType(type);
    pair.setExcludeReadOnlyFiles(excludeReadonly);
    pair.setExcludeHiddenFiles(excludeHidden);
    pair.setExcludeSystemFiles(excludeSystem);
    pair.setInclusionFilter(inclusionFilter);
    pair.setExclusionFilter(exclusionFilter);

    // continue closing the dialog
    QDialog::accept();
}


void PairEditionDialog::updateControls()
{
    // if the pair was not yet configured it still has a 0 id. we use this
    // information to prepare the window for a new pair creation.
    bool isNew = (pair.getId() == 0);

    if (isNew)
    {
        // configure the window title
        this->setWindowTitle(tr("New Pair"));
    }
    else
    {
        // configure the window title
        this->setWindowTitle(tr("Edit Pair"));

        // disable the folders edition
        this->ui->lineEditSourceFolder->setEnabled(false);
        this->ui->lineEditTargetFolder->setEnabled(false);
        this->ui->pushButtonSourceFolder->setEnabled(false);
        this->ui->pushButtonTargetFolder->setEnabled(false);
    }

    // update the ui contents according to the configured pair
    this->ui->lineEditName->setText(pair.getName());
    this->ui->comboBoxActive->setCurrentIndex(pair.isActive() ? 0 : 1);
    this->ui->lineEditSourceFolder->setText(pair.getSourcePath());
    this->ui->lineEditTargetFolder->setText(pair.getTargetPath());
    this->ui->comboBoxSynchType->setCurrentIndex(pair.getSynchronizationType() == S3sync::Model::Pair::ST_UNIDIRECTIONAL ? 0 : 1);

    // update the options
    this->ui->checkBoxReadonly->setChecked(pair.excludeReadOnlyFiles());
    this->ui->checkBoxHidden->setChecked(pair.excludeHiddenFiles());
    this->ui->checkBoxSystem->setChecked(pair.excludeSystemFiles());
    this->ui->lineEditInclusionFilter->setText(pair.getInclusionFilter());
    this->ui->lineEditExclusionFilter->setText(pair.getExclusionFilter());
}
