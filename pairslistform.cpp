/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "pairslistform.h"
#include "ui_pairslistform.h"
#include "connectorsingleton.h"
#include "data/iuser.h"
#include "paireditiondialog.h"
#include <QDebug>
#include <QMessageBox>

PairsListForm::PairsListForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PairsListForm)
{
    // call the automatically generated ui configuration method
    ui->setupUi(this);

    // customize the ui automatically generated
    customizeUi();

    // update the ui contents
    updateUi();
}

PairsListForm::~PairsListForm()
{
    delete ui;
}

void PairsListForm::createPair()
{
    // create a new pair to be edited
    S3sync::Model::Pair pair;

    // create the dialog for configuring the pair details
    PairEditionDialog dialog(pair, this);

    // if the chages were accepted
    if (dialog.exec() != QDialog::Accepted)
        return;

    // get the reference to the database user interface
    S3sync::Data::IUser* user = ConnectorSingleton::GetInstance()->getUser();

    // get a copy from the edited pair data
    S3sync::Model::Pair editedPair = dialog.getPair();

    // add it to the database
    if(!user->addPair(editedPair))
    {
        qDebug() << "The pair data was not correctly added to the database";
        return;
    }

    // update internal list
    pairs[editedPair.getId()] = editedPair;

    // create the correspondent item on the pairs list
    QListWidgetItem* item = new QListWidgetItem(editedPair.getName());
    item->setData(Qt::UserRole + ITD_ID,editedPair.getId());

    // add the newly created item to the list and select it
    // this will also trigger the selection of the single pair subform
    ui->pairListWidget->addItem(item);

    // if the current subform shows all of pairs
    if (ui->stackedWidget->currentIndex() == PAGE_ALL_PAIRS_SUBFORM)
    {
        // update the list
        ui->pageAll->updateUi(pairs);
    }
}

void PairsListForm::editPair(const S3sync::Model::Pair& pair)
{
    Q_ASSERT (pairs.contains(pair.getId()));

    // create the dialog for configuring the pair details
    PairEditionDialog dialog(pair, this);

    // if the chages were accepted
    if (dialog.exec() != QDialog::Accepted)
        return;

    // get the reference to the database user interface
    S3sync::Data::IUser* user = ConnectorSingleton::GetInstance()->getUser();

    // get a copy from the edited pair data
    S3sync::Model::Pair editedPair = dialog.getPair();

    // update the timecard on the database
    if (!user->modifyPair(editedPair))
    {
        qDebug() << "The pair data was not correctly saved at the database";
        return;
    }

    // update internal list
    pairs[editedPair.getId()] = editedPair;

    // look for the items that has that particular name
    QList<QListWidgetItem*> items = ui->pairListWidget->findItems(pair.getName(), Qt::MatchExactly);
    Q_ASSERT(items.count() > 0);

    // scan this list and delete the item with the pair id we are removing
    foreach (QListWidgetItem* item, items)
        if (item->data(Qt::UserRole + ITD_ID).toInt() == pair.getId())
            item->setText(editedPair.getName());

    // if the current subform shows all of pairs
    if (ui->stackedWidget->currentIndex() == PAGE_ALL_PAIRS_SUBFORM)
    {
        // update the list
        ui->pageAll->updateUi(pairs);
    }
    else
    {
        // update the single subform accordingly
        ui->pageSingle->updateUi(editedPair);
    }
}

void PairsListForm::deletePair(const S3sync::Model::Pair& pair)
{
    Q_ASSERT (pairs.contains(pair.getId()));

    // prepare the message box
    int res = QMessageBox::question(this,
                                    tr("A pair was selected for removal."),
                                    QString(tr("Do you really want to remove the pair \"%1\"?")).arg(pair.getName()),
                                    QMessageBox::Ok|QMessageBox::Cancel,
                                    QMessageBox::Cancel);

    // confirm the deletion
    if (res != QMessageBox::Ok)
        return;

    // get the reference to the database user interface
    S3sync::Data::IUser* user = ConnectorSingleton::GetInstance()->getUser();

    // remove it from the database
    S3sync::Model::Pair::Key id = pair.getId();
    if (!user->removePair(id))
    {
        qDebug() << "The pair data was not correctly deleted from the database";
        return;
    }

    // remove it from the private collection
    pairs.remove(id);

    // look for the items that has that particular name
    QList<QListWidgetItem*> items = ui->pairListWidget->findItems(pair.getName(), Qt::MatchExactly);
    Q_ASSERT(items.count() > 0);

    // scan this list and delete the item with the pair id we are removing
    foreach (QListWidgetItem* item, items)
        if (item->data(Qt::UserRole + ITD_ID).toInt() == pair.getId())
            delete item;

    // if the current subform shows all of pairs
    if (ui->stackedWidget->currentIndex() == PAGE_ALL_PAIRS_SUBFORM)
    {
        // update the list
        ui->pageAll->updateUi(pairs);
    }
}

void PairsListForm::on_pairListWidget_currentRowChanged(int currentRow)
{
    // check which page is currently selected on the stacked widget controller
    int currentIndex = ui->stackedWidget->currentIndex();

    // if the selected item is the first one on the pairs list
    // the ¨All Pairs¨ subform should be selected
    int newIndex = (currentRow <= 0) ? PAGE_ALL_PAIRS_SUBFORM :PAGE_PAIR_SUBFORM;

    // if we are supposed to show a new subform
    if (currentIndex != newIndex)
    {
        // change the stacked widget configuration accordingly
        ui->stackedWidget->setCurrentIndex(newIndex);
    }

    // if the new form to be shown is supposed to present the selected pair details
    if (newIndex == PAGE_PAIR_SUBFORM)
    {
        // get the reference to the selected item
        QListWidgetItem* selected = ui->pairListWidget->currentItem();

        // there should be a valid selection (this should never happen, but we never know)
        if (selected == 0)
        {
            qDebug() << "PairsListForm::on_pairListWidget_currentRowChanged: no item selected.";
            return;
        }

        // look at the pairs list for the item which key is
        // the same as the one stored on the qListWidgetItem
        S3sync::Model::Pair::Collection::ConstIterator it = pairs.find(selected->data(Qt::UserRole + ITD_ID).toInt());

        // this also should never happen because the list of pairs is constantly updated
        if (it == pairs.end())
        {
            qDebug() << "PairsListForm::on_pairListWidget_currentRowChanged: pair data found.";
            return;
        }

        // update the single pair sub form with the pair data
        ui->pageSingle->updateUi(*it);
    }
    else
    {
        // update the list of pairs
        ui->pageAll->updateUi(pairs);
    }
}

void PairsListForm::customizeUi()
{
    // connect both signals in order to forward the information sent by the subforms
    QObject::connect( ui->pageAll,SIGNAL(createPair()),
                this,SLOT(createPair()));
    QObject::connect( ui->pageAll,SIGNAL(editPair(S3sync::Model::Pair)),
                this,SLOT(editPair(S3sync::Model::Pair)));
    QObject::connect( ui->pageAll,SIGNAL(deletePair(S3sync::Model::Pair)),
                this,SLOT(deletePair(S3sync::Model::Pair)));
    QObject::connect( ui->pageAll,SIGNAL(previewPairs(S3sync::Model::Pair::Collection)),
                      this,SIGNAL(previewPairs(S3sync::Model::Pair::Collection)));
    QObject::connect( ui->pageAll, SIGNAL(runPairs(S3sync::Model::Pair::Collection)),
                      this,SIGNAL(runPairs(S3sync::Model::Pair::Collection)));

    QObject::connect( ui->pageSingle,SIGNAL(createPair()),
                this,SLOT(createPair()));
    QObject::connect( ui->pageSingle,SIGNAL(editPair(S3sync::Model::Pair)),
                this,SLOT(editPair(S3sync::Model::Pair)));
    QObject::connect( ui->pageSingle,SIGNAL(deletePair(S3sync::Model::Pair)),
                this,SLOT(deletePair(S3sync::Model::Pair)));
    QObject::connect( ui->pageSingle,SIGNAL(runPair(S3sync::Model::Pair)),
                this,SIGNAL(runPair(S3sync::Model::Pair)));
    QObject::connect( ui->pageSingle,SIGNAL(previewPair(S3sync::Model::Pair)),
                this,SIGNAL(previewPair(S3sync::Model::Pair)));
}

void PairsListForm::updateUi()
{
    // get the reference to the database user interface
    S3sync::Data::IUser* user = ConnectorSingleton::GetInstance()->getUser();

    // get the list of pairs from the database
    if (!user->getPairList(pairs))
    {
        qDebug() << "Error retrieving the list of pairs from the database";
        return;
    }

    // clear the current list (except the first item)
    while(ui->pairListWidget->count() > 1)
        delete ui->pairListWidget->item(1);

    // preselect the first item
    ui->pairListWidget->setCurrentRow(0);

    // scan the pairs on the list of pairs and add it to the list widget
    foreach(const S3sync::Model::Pair& pair,pairs)
    {
        // create the correspondent item
        QListWidgetItem* item = new QListWidgetItem(pair.getName());
        item->setData(Qt::UserRole + ITD_ID,pair.getId());

        // add the newly created item
        ui->pairListWidget->addItem(item);
    }
}
