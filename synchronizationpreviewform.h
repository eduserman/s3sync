/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SynchronizationPreviewForm_H
#define SynchronizationPreviewForm_H

#include <QWidget>
#include <QtConcurrent/QtConcurrent>
#include "model/pair.h"

namespace S3sync{
namespace Algorithm{
class ICompareCommand;
class BatchCommand;
}
}

namespace Ui {
class SynchronizationPreviewForm;
}

/**
 * @brief The SynchronizationDetailsForm class represents the windows
 * that presents the evolution of the synchronization process.
 */
class SynchronizationPreviewForm : public QWidget
{
    Q_OBJECT
    
public:

    /**
     * @brief Creates a new SynchronizationDetailsForm
     * @param parent The parent widget
     */
    explicit SynchronizationPreviewForm(QWidget *parent = 0);

    /**
     * Finishes the SynchronizationDetailsForm
     */
    ~SynchronizationPreviewForm();

    /**
     * @brief Update the ui for previewing purposes
     * @param pair the pair data
     */
    void updateUi(const S3sync::Model::Pair::Collection& pairs);

signals:

    /**
     * @brief Signal sent when the close button is clicked
     */
    void closePage();

    /**
     * @brief runPair
     * @param pairs
     * @param cmd
     */
    void runPreviewedPairs(const S3sync::Model::Pair::Collection& pairs, S3sync::Algorithm::BatchCommand* cmd);

private slots:

    /**
     * @brief on_pushButtonRun_clicked
     */
    void on_pushButtonRun_clicked();

    /**
     * @brief on_pushButtonStop_clicked
     */
    void on_pushButtonStop_clicked();

    /**
     * @brief on_pushButtonClose_clicked
     */
    void on_pushButtonClose_clicked();

    /**
     * @brief previewFinished
     */
    void previewFinished();

    /**
     * @brief updateStatusMessage
     */
    void updateStatusMessage(const QString& message);

private:

    /**
     * @brief The COLUMN_NAMES enum indicates the column indexes
     * at the table view.
     */
    enum COLUMN_NAMES{
        CN_ACTIVE,
        CN_OPERATION,
        CN_SOURCE_PATH,
        CN_SOURCE_FILE,
        CN_SOURCE_LAST,
        CN_TARGET_PATH,
        CN_TARGET_FILE,
        CN_TARGET_LAST
    };

    /// Reference to the automatically genereated ui form
    Ui::SynchronizationPreviewForm *ui;

    /// The pairs being previewed
    S3sync::Model::Pair::Collection pairs;

    /// Reference to the comparison algorithm
    S3sync::Algorithm::BatchCommand* previewBatch;

    /// Reference to the file system commands
    S3sync::Algorithm::BatchCommand* runBatch;

    /// The Qt future used for waiting the command result
    QFuture<int> future;

    /// The Qt future watcher used for waiting the command result
    QFutureWatcher<int> watcher;
};

#endif // SynchronizationPreviewForm_H
