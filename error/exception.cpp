/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "exception.h"

namespace S3sync{
namespace Error{

///////////////////////////////////////////////////////////////////////////////
// Exception class implementation

Exception::Exception()
{
}

Exception::Exception(const Exception& ex)
{
    Q_UNUSED(ex);
}

Exception::~Exception() throw ()
{
}

///////////////////////////////////////////////////////////////////////////////
// DataException class implementation

DataException::DataException(const QString& d):
    details(d)
{
}

DataException::DataException(const DataException& ex):
    Exception(ex),
    details(ex.details)
{
    Q_UNUSED(ex);
}

DataException::~DataException() throw ()
{
}

///////////////////////////////////////////////////////////////////////////////
// AlgorithmException class implementation

AlgorithmException::AlgorithmException(const QString& d):
    details(d)
{
}

AlgorithmException::AlgorithmException(const AlgorithmException& ex):
    Exception(ex),
    details(ex.details)
{
    Q_UNUSED(ex);
}

AlgorithmException::~AlgorithmException() throw ()
{
}

}
}
