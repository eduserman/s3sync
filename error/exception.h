/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef S3SYNC_EXCEPTION_H
#define S3SYNC_EXCEPTION_H

#include <QtCore>
#include <stdexcept>

namespace S3sync{
namespace Error{

/**
  * The Exception is the base class for all application custom exceptions.
  */
class Exception : public std::exception
{
public:

    /**
      * Creates a new Exception instance
      */
    Exception();

    /**
      * Creates a new Exception instance based on an existing one
      * @param ex the exception this instance should be based on
      */
    Exception(const Exception& ex);

    /**
      * Finishes the Exception
      */
    virtual ~Exception() throw ();
};

/**
  * The DataException is the base class for all database related exception
  */
class DataException : public Exception
{
public:

    /**
      * Creates a new DataException instance
      */
    DataException(const QString& d);

    /**
      * Creates a new DataException instance based on an existing one
      * @param ex the DataException this instance should be based on
      */
    DataException(const DataException& ex);

    /**
      * Finishes the DataException
      */
    virtual ~DataException() throw ();

private:

    /// String, that should come from the database engine, that contains the error details
    QString details;
};

/**
  * The AlgorithmException is the base class for all algorithm related exception
  */
class AlgorithmException : public Exception
{
public:

    /**
      * Creates a new AlgorithmException instance
      */
    AlgorithmException(const QString& d);

    /**
      * Creates a new AlgorithmException instance based on an existing one
      * @param ex the AlgorithmException this instance should be based on
      */
    AlgorithmException(const AlgorithmException& ex);

    /**
      * Finishes the AlgorithmException
      */
    virtual ~AlgorithmException() throw ();

private:

    /// String, that should come from the database engine, that contains the error details
    QString details;
};

}
}

#endif // S3SYNC_EXCEPTION_H
