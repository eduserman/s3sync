/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PAIRSLISTFORM_H
#define PAIRSLISTFORM_H

#include <QWidget>
#include "model/pair.h"

namespace Ui {
class PairsListForm;
}

class PairsListForm : public QWidget
{
    Q_OBJECT
    
public:

    /**
     * @brief Creates a new PairsListForm
     * @param parent the parent widget
     */
    explicit PairsListForm(QWidget *parent = 0);

    /**
      * @brief Finishes the PairsListForm
      **/
    ~PairsListForm();

signals:

    /**
      * Signal that is sent when a pair should be ran
      * @param key the pair identification key
      */
    void runPair(const S3sync::Model::Pair& pair);

    /**
      * Signal that is sent when a pair should be previewed
      * @param key the pair identification key
      */
    void previewPair(const S3sync::Model::Pair& pair);

    /**
     * Signal that will be sent when the "preview all" button is pressed.
     */
    void previewPairs(const S3sync::Model::Pair::Collection& pairs);

    /**
     * Signal that will be sent when the "run all" button is pressed.
     */
    void runPairs(const S3sync::Model::Pair::Collection& pairs);

private slots:

    /**
      * Signal that is sent when a pair should be created created
      * @param key the pair identification key
      */
    void createPair();

    /**
      * Signal that is sent when a pair should be edited
      * @param key the pair identification key
      */
    void editPair(const S3sync::Model::Pair& pair);

    /**
      * Signal that is sent when a pair should updated
      * @param key the pair identification key
      */
    void deletePair(const S3sync::Model::Pair& pair);

    /**
     * @brief Signal that is sent when the row selection is changed
     * @param currentRow the new row.
     */
    void on_pairListWidget_currentRowChanged(int currentRow);

private:

    enum {
        /// Index of the ¨All Pairs¨ subform on the stacked widget
        PAGE_ALL_PAIRS_SUBFORM = 0,

        /// Index of the single pair details subform on the stacked widget
        PAGE_PAIR_SUBFORM = 1
    };

    /// Enumeration that indicates content of the QUserData stored
    /// on the first column
    enum ITEM_DATA
    {
        ITD_ID
    };

    /**
      * @brief Customize the ui generated by the QtDesigner
      */
    void customizeUi();

    /**
      * This method allows its users to request the update of the ui contents.
      */
    void updateUi();

private:

    /// Reference to the automatically genereated ui form
    Ui::PairsListForm *ui;

    /// The list of pairs being edited
    S3sync::Model::Pair::Collection pairs;
};

#endif // PAIRSLISTFORM_H
