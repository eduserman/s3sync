/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CONNECTORSINGLETON_H
#define CONNECTORSINGLETON_H

#include <QObject>
#include "data/iuser.h"
#include "error/exception.h"

namespace S3sync{
namespace Data {
class IConnector;
}
}

/**
 * @brief The ConnectorSingleton class owns the database interface.
 */
class ConnectorSingleton : public QObject
{
    Q_OBJECT

    /**
     * @brief Creates a new ConnectorSingleton
     * @param parent the parent object
     */
    explicit ConnectorSingleton(QObject *parent = 0);

public:

    /**
     * @brief The singleton read accessor
     * @return The singleton unique instance
     */
    static ConnectorSingleton* GetInstance();

    /**
      * @brief Initializes the database interface.
      */
    void start() throw (S3sync::Error::DataException);

    /**
      * @brief Finishes the database interface.
      */
    void stop() throw ();

    /**
     * @brief Get the reference to the user interface.
     * @return The user interface
     */
    S3sync::Data::IUser* getUser();

signals:
    
public slots:
    
private:

    /// The user interface
    S3sync::Data::IUser* user;

    /// The connector
    S3sync::Data::IConnector* connector;
};

#endif // CONNECTORSINGLETON_H
