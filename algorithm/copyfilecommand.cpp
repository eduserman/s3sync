/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "copyfilecommand.h"

namespace S3sync{
namespace Algorithm{

CopyFileCommand::CopyFileCommand(const QFileInfo& s, const QFileInfo& t, QObject *parent) :
    IFileSystemCommand(s, t, parent)
{
}

int CopyFileCommand::run() throw(Error::AlgorithmException)
{
    // make sure we have a valid file to be copied
    if (!source.isFile())
        throw Error::AlgorithmException(QString(tr("CopyFileCommand: Invalid source path (%1)")).arg(source.absoluteFilePath()));
    if (!source.exists())
        throw Error::AlgorithmException(QString(tr("CopyFileCommand: Source file not found (%1)")).arg(source.absoluteFilePath()));
    if (!target.exists())
        throw Error::AlgorithmException(QString(tr("CopyFileCommand: Target path not found (%1)")).arg(target.absoluteFilePath()));
    if (!target.isDir())
        throw Error::AlgorithmException(QString(tr("CopyFileCommand: Target path must point to a directory (%1)")).arg(target.absoluteFilePath()));

    // update the target to form the complete target name
    target = QFileInfo(QDir(target.absoluteFilePath()),source.fileName());

    // send status message
    emit statusMessage(QString(tr("Copying file %1 to %2")).
                       arg(source.absoluteFilePath()).
                       arg(target.absoluteFilePath()));

    // copy it
    return QFile::copy(source.absoluteFilePath(),target.absoluteFilePath()) &&
           setLastModified(source,target) ? 0 : 1;
}

QString CopyFileCommand::getDescription() const
{
    return QString(tr("Copy (%1) to (%2)"))
            .arg(source.absoluteFilePath())
            .arg(target.absoluteFilePath());
}

}
}
