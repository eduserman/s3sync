/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "unidirectionalcompare.h"
#include "copyfilecommand.h"
#include "overwritefilecommand.h"
#include "deletefilecommand.h"
#include "copydircommand.h"
#include "deletedircommand.h"
#include "updatelastmodificationcommand.h"

namespace S3sync{
namespace Algorithm{

UnidirectionalCompare::UnidirectionalCompare(const QFileInfo& s, const QFileInfo& t, const ComparisonDetails& d, BatchCommand* b, QObject *parent) :
    ICompareCommand(s,t,d,b,parent)
{
}

QString UnidirectionalCompare::getDescription() const
{
    return QString(tr("Unidirectional Comparison Algorithm: Source path (%1) Target path (%2)"))
            .arg(source.absoluteFilePath())
            .arg(target.absoluteFilePath());
}

void UnidirectionalCompare::fileNotChanged(const QFileInfo& s, const QFileInfo& t)
{
    Q_UNUSED(s);
    Q_UNUSED(t);
    // File not changed...
}

void UnidirectionalCompare::fileContentsChanged(const QFileInfo& s, const QFileInfo& t)
{
    addCommand(new OverwriteFileCommand(s,t));
}

void UnidirectionalCompare::fileTypeChanged(const QFileInfo& s, const QFileInfo& t)
{
    if (t.isFile())
    {
        addCommand(new DeleteFileCommand(t));
    }
    else if (t.isDir())
    {
        addCommand(new DeleteDirCommand(t));
        addCommand(new CopyFileCommand(s,t));
    }
}

void UnidirectionalCompare::fileLastModificationChanged(const QFileInfo& s, const QFileInfo& t)
{
    addCommand(new UpdateLastModificationCommand(s,t));
}

void UnidirectionalCompare::fileMissingAtSource(const QFileInfo& s, const QFileInfo& t)
{
    Q_UNUSED(s);
    addCommand(new DeleteFileCommand(t));
}

void UnidirectionalCompare::fileMissingAtDestination(const QFileInfo& s, const QFileInfo& t)
{
    addCommand(new CopyFileCommand(s,t));
}

void UnidirectionalCompare::directoryNotChanged(const QFileInfo& s, const QFileInfo& t)
{
    Q_UNUSED(s);
    Q_UNUSED(t);
    // Directory not changed...
}

void UnidirectionalCompare::directoryContentsChanged(const QFileInfo& s, const QFileInfo& t)
{
    Q_UNUSED(s);
    Q_UNUSED(t);
}

void UnidirectionalCompare::directoryTypeChanged(const QFileInfo& s, const QFileInfo& t)
{
    if (t.isFile())
    {
        addCommand(new DeleteFileCommand(t));
    }
    else if (t.isDir())
    {
        addCommand(new DeleteDirCommand(t));
        addCommand(new CopyDirCommand(s,t));
    }
}

void UnidirectionalCompare::directoryMissingAtSource(const QFileInfo& s, const QFileInfo& t)
{
    Q_UNUSED(s);
    addCommand(new DeleteDirCommand(t));
}

void UnidirectionalCompare::directoryMissingAtDestination(const QFileInfo& s, const QFileInfo& t)
{
    addCommand(new CopyDirCommand(s,t));
}

}
}
