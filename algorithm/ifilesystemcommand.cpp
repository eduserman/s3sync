/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ifilesystemcommand.h"
#include <time.h>
#include <utime.h>

namespace S3sync{
namespace Algorithm{

IFileSystemCommand::IFileSystemCommand(const QFileInfo& s, const QFileInfo& t, QObject *parent) :
    ICommand(parent),
    source(s),
    target(t)
{
}

void IFileSystemCommand::stop() throw(Error::AlgorithmException)
{
    /// for now, file system commands cannot be stopped
    /// it might be a good idea to create an implementation
    /// for the copy / delete directoris commands...
}

bool IFileSystemCommand::setLastModified(const QFileInfo& source, const QFileInfo& target)
{
    // reset the timebuffer
    struct utimbuf times;
    memset(&times,0,sizeof(struct utimbuf));

    // the modification and last access time will be updated with the information passed as parameter
    times.modtime = source.lastModified().toTime_t();
    times.actime  = source.lastRead().toTime_t();

    // update the file timestamps from both source and target to make sure they are identical
    return (utime(source.absoluteFilePath().toLocal8Bit().constData(),&times) == 0) &&
           (utime(target.absoluteFilePath().toLocal8Bit().constData(),&times) == 0);
}

}
}

