/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "deletefilecommand.h"

namespace S3sync{
namespace Algorithm{

DeleteFileCommand::DeleteFileCommand(const QFileInfo& t, QObject *parent) :
    IFileSystemCommand(QFileInfo(), t, parent)
{
}

int DeleteFileCommand::run() throw(Error::AlgorithmException)
{
    // make sure we have a valid file to be copied
    if (!target.isFile())
        throw Error::AlgorithmException(QString(tr("DeleteFileCommand: Invalid target path (%1)")).arg(target.absoluteFilePath()));
    if (!target.exists())
        throw Error::AlgorithmException(QString(tr("DeleteFileCommand: Target file not found (%1)")).arg(target.absoluteFilePath()));

    // send status message
    emit statusMessage(QString(tr("Deleting file %1")).
                       arg(target.absoluteFilePath()));

    // try to remove it
    return QFile::remove(target.absoluteFilePath()) ? 0 : 1;
}

QString DeleteFileCommand::getDescription() const
{
    return QString(tr("Delete (%1)"))
            .arg(target.absoluteFilePath());
}

}
}
