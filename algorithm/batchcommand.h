/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef BATCHCOMMAND_H
#define BATCHCOMMAND_H

#include "icommand.h"

class QFileInfo;

namespace S3sync{
namespace Algorithm{

/**
 * @brief The BatchCommand is the ICommand implementation
 * that corresponds to a list of commands to be executed.
 */
class BatchCommand : public ICommand
{
    Q_OBJECT

public:

    /// Definition of the collection of commands that will be
    /// prepared by the inherited comparison commands.
    typedef QList<ICommand*> CommandCollection;

    /**
     * @brief Creates a new BatchCommand
     * @param parent the parent object
     */
    explicit BatchCommand(QObject *parent = 0);

    /**
     * @brief Finishes the batch command
     */
    virtual ~BatchCommand();

    /**
     * @brief The run method is the central point for the ICommand interface.
     * It should trigger, on the inherited classes, the correspondent command
     * activities.
     *
     * @throws The inherited classes should throw an AlgorithmException in
     * case a critical failure prohibits the command execution.
     */
    virtual int run() throw(Error::AlgorithmException);

    /**
     * @brief Enables the user to stop the command execution
     *
     * @throws The inherited classes should throw an AlgorithmException in
     * case a critical failure prohibits the command cancellation.
     */
    virtual void stop() throw(Error::AlgorithmException);

    /**
     * @brief Read accessor for the command textual description. This
     * will be used by the UI when presenting the commands to the users.
     *
     * @returns the command textual description.
     */
    virtual QString getDescription() const;

    /**
     * @brief The commands read accessor
     * @return the reference to the list of commands
     */
    inline const CommandCollection& getCommands() const;

    /**
     * @brief Add a cmd to the internal collection
     * @param cmd the command to be added
     */
    inline void addCommand(ICommand* cmd);

    /**
     * @brief Remove a command from the internal list
     * @param cmd the command to be removed
     */
    inline void removeCommand(ICommand* cmd);

    /**
     * @brief Remove all commands from the internal list
     */
    inline void removeAllCommands();

signals:

public slots:

private:

    /// Flag used to stop the command execution
    volatile bool stopRunning;

    /// List of commands to be executed
    CommandCollection commands;
};

inline const BatchCommand::CommandCollection& BatchCommand::getCommands() const
{
    return commands;
}

inline void BatchCommand::addCommand(ICommand* cmd)
{
    if (cmd){
        QObject::connect(cmd,SIGNAL(statusMessage(QString)),this,SIGNAL(statusMessage(QString)));
        commands.push_back(cmd);
    }
}

inline void BatchCommand::removeCommand(ICommand* cmd)
{
    if (cmd){
        QObject::disconnect(cmd,SIGNAL(statusMessage(QString)),this,SIGNAL(statusMessage(QString)));
        commands.removeOne(cmd);
    }
}

inline void BatchCommand::removeAllCommands()
{
    commands.clear();
}

}
}

#endif // BATCHCOMMAND_H
