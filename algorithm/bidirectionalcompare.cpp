/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "bidirectionalcompare.h"
#include "copyfilecommand.h"
#include "overwritefilecommand.h"
#include "deletefilecommand.h"
#include "copydircommand.h"
#include "deletedircommand.h"
#include "updatelastmodificationcommand.h"

namespace S3sync{
namespace Algorithm{

BidirectionalCompare::BidirectionalCompare(const QFileInfo& s, const QFileInfo& t, const ComparisonDetails& d, BatchCommand* b, QObject *parent) :
    ICompareCommand(s,t,d,b,parent)
{
}

QString BidirectionalCompare::getDescription() const
{
    return QString(tr("Biderectional Comparison Algorithm: Source path (%1) Target path (%2)"))
            .arg(source.absoluteFilePath())
            .arg(target.absoluteFilePath());
}

void BidirectionalCompare::fileNotChanged(const QFileInfo& s, const QFileInfo& t)
{
    Q_UNUSED(s);
    Q_UNUSED(t);
}

void BidirectionalCompare::fileContentsChanged(const QFileInfo& s, const QFileInfo& t)
{
    if (s.lastModified() > t.lastModified())
    {
        addCommand(new OverwriteFileCommand(s,t));
    }
    else
    {
        addCommand(new OverwriteFileCommand(t,s));
    }
}

void BidirectionalCompare::fileTypeChanged(const QFileInfo& s, const QFileInfo& t)
{
    if (s.lastModified() > t.lastModified())
    {
        if (t.isFile())
        {
            addCommand(new DeleteFileCommand(t));
        }
        else if (t.isDir())
        {
            addCommand(new DeleteDirCommand(t));
            addCommand(new CopyFileCommand(s,t));
        }
    }
    else
    {
        if (s.isFile())
        {
            addCommand(new DeleteFileCommand(s));
        }
        else if (s.isDir())
        {
            addCommand(new DeleteDirCommand(s));
            addCommand(new CopyFileCommand(t,s));
        }
    }
}

void BidirectionalCompare::fileLastModificationChanged(const QFileInfo& s, const QFileInfo& t)
{
    if (s.lastModified() > t.lastModified())
    {
        addCommand(new UpdateLastModificationCommand(t,s));
    }
    else
    {
        addCommand(new UpdateLastModificationCommand(s,t));
    }
}

void BidirectionalCompare::fileMissingAtSource(const QFileInfo& s, const QFileInfo& t)
{
    addCommand(new CopyFileCommand(t,s));
}

void BidirectionalCompare::fileMissingAtDestination(const QFileInfo& s, const QFileInfo& t)
{
    addCommand(new CopyFileCommand(s,t));
}

void BidirectionalCompare::directoryNotChanged(const QFileInfo& s, const QFileInfo& t)
{
    Q_UNUSED(s);
    Q_UNUSED(t);
    // Directory not changed...
}

void BidirectionalCompare::directoryContentsChanged(const QFileInfo& s, const QFileInfo& t)
{
    Q_UNUSED(s);
    Q_UNUSED(t);
}

void BidirectionalCompare::directoryTypeChanged(const QFileInfo& s, const QFileInfo& t)
{
    if (s.lastModified() > t.lastModified())
    {
        if (t.isFile())
        {
            addCommand(new DeleteFileCommand(t));
        }
        else if (t.isDir())
        {
            addCommand(new DeleteDirCommand(t));
            addCommand(new CopyDirCommand(s,t));
        }
    }
    else
    {
        if (s.isFile())
        {
            addCommand(new DeleteFileCommand(s));
        }
        else if (s.isDir())
        {
            addCommand(new DeleteDirCommand(s));
            addCommand(new CopyDirCommand(t,s));
        }
    }
}

void BidirectionalCompare::directoryMissingAtSource(const QFileInfo& s, const QFileInfo& t)
{
    addCommand(new CopyDirCommand(t,s));
}

void BidirectionalCompare::directoryMissingAtDestination(const QFileInfo& s, const QFileInfo& t)
{
    addCommand(new CopyDirCommand(s,t));
}

}
}
