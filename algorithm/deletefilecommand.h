/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef DeleteFileCommand_H
#define DeleteFileCommand_H

#include "ifilesystemcommand.h"

namespace S3sync{
namespace Algorithm{

/**
 * @brief The DeleteDirCommand class implements the deletion of a file.
 */
class DeleteFileCommand : public IFileSystemCommand
{
    Q_OBJECT

public:

    /**
     * @brief Creates a new DeleteFileCommand.
     * @param target The target path.
     * @param parent The parent widget.
     */
    explicit DeleteFileCommand(const QFileInfo& target, QObject *parent = 0);

    /**
     * @brief The run method is the central point for the ICommand interface.
     * It should trigger, on the inherited classes, the correspondent command
     * activities.
     *
     * @throws The inherited classes should throw an AlgorithmException in
     * case a critical failure prohibits the command execution.
     */
    virtual int run() throw(Error::AlgorithmException);

    /**
     * @brief Read accessor for the command textual description. This
     * will be used by the UI when presenting the commands to the users.
     *
     * @returns the command textual description.
     */
    virtual QString getDescription() const;

signals:

public slots:

private:

};

}
}

#endif // DeleteFileCommand_H
