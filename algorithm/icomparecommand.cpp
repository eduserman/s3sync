/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "icomparecommand.h"
#include "batchcommand.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QDateTime>
#include <QDebug>
#include <algorithm>

namespace S3sync{
namespace Algorithm{

class FileInfoCompare
{
public:

    FileInfoCompare(const QFileInfo& toCompare):toCompare(toCompare)
    {
    }

    bool operator()(const QFileInfo other)
    {
        return (other.fileName() == toCompare.fileName());
    }
private:

    const QFileInfo& toCompare;
};

ComparisonDetails::ComparisonDetails():
    excludeReadOnly(false),
    excludeHidden(false),
    excludeSystem(false),
    inclusionFilter("*"),
    exclusionFilter()
{
}

ComparisonDetails::ComparisonDetails(bool ero, bool eh, bool es, const QString& inf, const QString& ef):
    excludeReadOnly(ero),
    excludeHidden(eh),
    excludeSystem(es),
    inclusionFilter(inf),
    exclusionFilter(ef)
{

}

ICompareCommand::ICompareCommand(const QFileInfo& s, const QFileInfo& t, const ComparisonDetails& d, BatchCommand* b, QObject *parent) :
    ICommand(parent),
    source(s),
    target(t),
    details(d),
    entryInfo(),
    entryCount(1),
    entryIndex(0),
    batch(b),
    stopRunning(false)
{
    // create a batch if none is provided
    if (!batch)
        batch = new BatchCommand(this);
}

ICompareCommand::~ICompareCommand()
{
    // there is no need for deleting the batch commands once it was added as a ICompareCommand child
    // or it was provided as an input parameter (on this case we hope the caller will release it)
    // delete batch;
    // batch = 0;
}

int ICompareCommand::run() throw(Error::AlgorithmException)
{
    // make sure both source and target directories are valid
    if (!source.isDir())
        throw Error::AlgorithmException(QString(tr("ICompareCommand: Invalid source path (%1)")).arg(source.absoluteFilePath()));
    if (!target.isDir())
        throw Error::AlgorithmException(QString(tr("ICompareCommand: Invalid target path (%1)")).arg(target.absoluteFilePath()));

    // make sure both source and target directories exists on the file system
    if (!source.exists())
        throw Error::AlgorithmException(QString(tr("ICompareCommand: Source directory not found (%1)")).arg(source.absoluteFilePath()));
    if (!target.exists())
        throw Error::AlgorithmException(QString(tr("ICompareCommand: Target directory not found (%1)")).arg(target.absoluteFilePath()));

    // safelly reset the current source folder
    currentSourceFolderLock.lockForWrite();
    entryInfo  = source;
    entryCount = 1;
    entryIndex = 0;
    currentSourceFolderLock.unlock();

    // compare source and target directories
    compareDirectories(source,target);

    // we always return 0 because there is no situation found that
    // might be classified as an error.
    return 0;
}

void ICompareCommand::stop() throw(Error::AlgorithmException)
{
    // set the flag to true
    stopRunning = true;
}

void ICompareCommand::readCurrentEntryInfo(QFileInfo& entry, int& count, int& index)
{
    currentSourceFolderLock.lockForRead();
    entry = entryInfo;
    count = entryCount;
    index = entryIndex;
    currentSourceFolderLock.unlock();
}

int ICompareCommand::compareFiles(const QFileInfo& source, const QFileInfo& target)
{
    int result = 0;

    // send status message
    emit statusMessage(QString("Comparing files %1 and %2").
                       arg(source.absoluteFilePath()).
                       arg(target.absoluteFilePath()));

    // if the file sizes are different, there is no need for any additional comparison
    if (source.size() != target.size())
        return 1;

    // open the files for comparison
    QFile sourceFile(source.filePath());
    QFile targetFile(target.filePath());
    if (!sourceFile.open(QIODevice::ReadOnly | QIODevice::Text))
        return 1;
    if (!targetFile.open(QIODevice::ReadOnly | QIODevice::Text))
        return 1;

    // allocate the buffers for reading
    qint64 bufferSize = 64 * 1024;
    char* sourceBuffer = new char[bufferSize];
    char* targetBuffer = new char[bufferSize];

#ifdef QT_DEBUG
    memset(sourceBuffer,0,bufferSize);
    memset(targetBuffer,0,bufferSize);
#endif

    // compare the file contents
    do
    {
        // read a 64K chunck from both files
        qint64 sourceRead = sourceFile.read(sourceBuffer, bufferSize);
        qint64 targetRead = targetFile.read(targetBuffer, bufferSize);

        // if the amount of read data or the data contents are different
        if ((sourceRead != targetRead) || memcmp(sourceBuffer,targetBuffer,sourceRead))
        {
            result = 1;
            break;
        }
    }
    while (!sourceFile.atEnd() || !targetFile.atEnd());

    // release the reading buffers
    delete [] sourceBuffer;
    delete [] targetBuffer;

    return result;
}

int ICompareCommand::compareDirectories(const QFileInfo& source, const QFileInfo& target)
{
    int result = 0;

    // open the directories for comparison
    QDir sourceDir(source.filePath());
    QDir targetDir(target.filePath());    

    // send status message
    emit statusMessage(QString("Comparing directories %1 and %2").
                       arg(sourceDir.absolutePath()).
                       arg(targetDir.absolutePath()));

    // configure the filter selection all files/dir/symbolik links
    QDir::Filters filters = QDir::AllEntries | (QDir::Hidden | QDir::System) | QDir::NoDotAndDotDot;

    // if we want to remove the read only files, we add the read and write flags
    if (details.excludeReadOnlyFiles())
        filters |= (QDir::Readable|QDir::Writable);

    // exclude the hidden and system flags as needed
    if (details.excludeHiddenFiles())
        filters &= static_cast<QDir::Filter>(~QDir::Hidden);
    if (details.excludeSystemFiles())
        filters &= static_cast<QDir::Filter>(~QDir::System);

    // get the inclusion filter
    QStringList inclusionFilter = details.getInclusionFilter().split(';');

    // get the list of files and directories from the current path
    QFileInfoList sourceEntries = sourceDir.entryInfoList(inclusionFilter,filters);
    QFileInfoList targetEntries = targetDir.entryInfoList(inclusionFilter,filters);

    // update the amount of items found
    currentSourceFolderLock.lockForWrite();
    entryCount += sourceEntries.length();
    currentSourceFolderLock.unlock();

    for(QFileInfoList::Iterator sourceEntryIt = sourceEntries.begin(); sourceEntryIt != sourceEntries.end(); ++sourceEntryIt)
    {
        // stop running if requested
        if (stopRunning)
            break;

        // get the reference to the current file entry
        QFileInfo sourceEntry = *sourceEntryIt;

        // safelly update the current source folder
        currentSourceFolderLock.lockForWrite();
        entryInfo = sourceEntry;
        entryIndex++;
        currentSourceFolderLock.unlock();

        // try to find the current entry on the source list at the target one.
        QFileInfoList::Iterator targetEntryIt = std::find_if(targetEntries.begin(), targetEntries.end(), FileInfoCompare(sourceEntry));

        // if the entry was not found...
        if (targetEntryIt == targetEntries.end())
        {
            // if the entry on the source directory is a file
            if (sourceEntry.isFile())
            {
                fileMissingAtDestination(sourceEntry, target);
                result = 1;
            }
            // or if it is a directory
            else if (sourceEntry.isDir())
            {
                directoryMissingAtDestination(sourceEntry, target);
                result = 1;
            }

            // next one...
            continue;
        }

        // file present
        QFileInfo targetEntry = *targetEntryIt;

        // this entry is now free to be removed from the target list
        targetEntries.erase(targetEntryIt);

        // check if the file was NOT modified
        if (sourceEntry.lastModified() == targetEntry.lastModified())
        {
            // if the entry on the source directory is a file
            if (sourceEntry.isFile())
            {
                // File not changed
                fileNotChanged(sourceEntry,targetEntry);

                // next one...
                continue;
            }
        }

        // if the entry on the source directory is a file
        if (sourceEntry.isFile())
        {
            // and the entry on the target directory is also a file
            if (targetEntry.isFile())
            {
                // check if they are the same
                if (compareFiles(sourceEntry,targetEntry) == 0)
                {
                    // File not changed
                    fileLastModificationChanged(sourceEntry,targetEntry);
                }
                else
                {
                    // File contents changed
                    fileContentsChanged(sourceEntry,targetEntry);
                    result = 1;
                }
            }
            else
            {
                // File type changed
                fileTypeChanged(sourceEntry,targetEntry);
                result = 1;
            }
        }
        // or if it is a directory
        else if (sourceEntry.isDir())
        {
            // and the entry on the target directory is also a directory
            if (targetEntry.isDir())
            {
                // check if they are the same
                if (compareDirectories(sourceEntry,targetEntry) == 0)
                {
                    // Directory not changed
                    directoryNotChanged(sourceEntry,targetEntry);
                }
                else
                {
                    // Directory contents changed
                    directoryContentsChanged(sourceEntry,targetEntry);
                    result = 1;
                }
            }
            else
            {
                // Directory type changed
                directoryTypeChanged(sourceEntry,targetEntry);
                result = 1;
            }
        }
    }

    // this items were found on the target and are not present at the source.
    for(QFileInfoList::Iterator targetEntryIt = targetEntries.begin(); targetEntryIt != targetEntries.end();  ++targetEntryIt)
    {
        // stop running if requested
        if (stopRunning)
            break;

        // deleted
        QFileInfo targetEntry = *targetEntryIt;

        // if the entry on the target directory is a file
        if (targetEntry.isFile())
        {
            fileMissingAtSource(source,targetEntry);
            result = 1;
        }
        // or if it is a directory
        else if (targetEntry.isDir())
        {
            directoryMissingAtSource(source,targetEntry);
            result = 1;
        }
    }

    return result;
}

void ICompareCommand::addCommand(ICommand* cmd)
{
    // add the command to the batch
    batch->addCommand(cmd);
}

}
}
