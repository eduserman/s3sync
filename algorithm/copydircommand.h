/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef COPYDIRCOMMAND_H
#define COPYDIRCOMMAND_H

#include "ifilesystemcommand.h"

namespace S3sync{
namespace Algorithm{

/**
 * @brief The CopyDirCommand class implements the copy the directory
 * contents from the source path to the target path.
 */
class CopyDirCommand : public IFileSystemCommand
{
    Q_OBJECT

public:

    /**
     * @brief Creates a new CopyDirCommand.
     * @param source The source path.
     * @param target The target path.
     * @param parent The parent widget.
     */
    explicit CopyDirCommand(const QFileInfo& source, const QFileInfo& target, QObject *parent = 0);

    /**
     * @brief The run method is the central point for the ICommand interface.
     * It should trigger, on the inherited classes, the correspondent command
     * activities.
     *
     * @throws The inherited classes should throw an AlgorithmException in
     * case a critical failure prohibits the command execution.
     */
    virtual int run() throw(Error::AlgorithmException);

    /**
     * @brief Enables the user to stop the command execution
     *
     * @throws The inherited classes should throw an AlgorithmException in
     * case a critical failure prohibits the command cancellation.
     */
    virtual void stop() throw(Error::AlgorithmException);

    /**
     * @brief Read accessor for the command textual description. This
     * will be used by the UI when presenting the commands to the users.
     *
     * @returns the command textual description.
     */
    virtual QString getDescription() const;

signals:

public slots:

private:

    /**
     * @brief Copy the contents of a source directory to the target one.
     *
     * @param source the source directory
     * @param target the target directory
     * @returns true if the copy was successfully executed
     */
    bool copyDir(const QDir& source, const QDir& target);

    /// Flag used to stop the command execution
    volatile bool stopRunning;
};

}
}

#endif // COPYDIRCOMMAND_H
