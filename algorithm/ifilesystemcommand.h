/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef IFILESYSTEMCOMMAND_H
#define IFILESYSTEMCOMMAND_H

#include "icommand.h"

namespace S3sync{
namespace Algorithm{

/**
 * @brief The IFileSystemCommand interface defines the common behaviour for
 * all commands that manage file system resources like files and directories.
 */
class IFileSystemCommand : public ICommand
{
    Q_OBJECT

public:

    /**
     * @brief Creates a new IFileSystemCommand
     * @param source the source path
     * @param target the target path
     * @param parent the parent QObject
     */
    explicit IFileSystemCommand(const QFileInfo& source, const QFileInfo& target, QObject *parent = 0);

    /**
     * @brief Enables the user to stop the command execution
     *
     * @throws The inherited classes should throw an AlgorithmException in
     * case a critical failure prohibits the command cancellation.
     */
    virtual void stop() throw(Error::AlgorithmException);

    /**
     * @brief Source path read accessor
     * @return the source path
     */
    inline const QFileInfo& getSource() const;

    /**
     * @brief Target path read accessor
     * @return the target path
     */
    inline const QFileInfo& getTarget() const;

protected:

    /**
     * @brief update target's last modification date/time according to the source one
     * @param source the file from which the date/time info are going to be read
     * @param target the file to be updated
     * @return true if successfully modified
     */
    bool setLastModified(const QFileInfo& source, const QFileInfo& target);

signals:

public slots:

protected:

    /// Source path to be analyzed
    QFileInfo source;

    /// Target path to be analyzed
    QFileInfo target;
};

inline const QFileInfo& IFileSystemCommand::getSource() const
{
    return source;
}

inline const QFileInfo& IFileSystemCommand::getTarget() const
{
    return target;
}

}
}

#endif // IFILESYSTEMCOMMAND_H
