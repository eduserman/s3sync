/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ICOMPARE_H
#define ICOMPARE_H

#include "icommand.h"

class QFileInfo;

namespace S3sync{
namespace Algorithm{

class BatchCommand;

/**
 * @brief The ComparisonDetails represents the possible fine tunning
 * options for a comparison command.
 */
class ComparisonDetails
{
public:

    /**
     * @brief Creates the default ComparisonDetails
     */
    ComparisonDetails();

    /**
     * @brief Creates a ComparisonDetails according to the input parameters
     * @param excludeReadOnly the exclude readonly flag
     * @param excludeHidden the exclude hidden flag
     * @param excludeSystem the exclude system flag
     * @param inclusionFilter the inclusion filter
     * @param exclusionFilter the exclusion filter
     */
    ComparisonDetails(bool excludeReadOnly, bool excludeHidden, bool excludeSystem, const QString& inclusionFilter, const QString& exclusionFilter);

    /**
     * @brief ExcludeReadOnlyFiles flag read accessor
     * @return the flag state
     */
    inline bool excludeReadOnlyFiles() const;

    /**
     * @brief ExcludeReadOnlyFiles flag write accessor
     * @param value the new flag state
     */
    inline void setExcludeReadOnlyFiles(bool value);

    /**
     * @brief ExcludeHiddenFiles flag read accessor
     * @return the flag state
     */
    inline bool excludeHiddenFiles() const;

    /**
     * @brief ExcludeHiddenFiles flag write accessor
     * @param value the new flag state
     */
    inline void setExcludeHiddenFiles(bool value);

    /**
     * @brief ExcludeSystemFiles flag read accessor
     * @return the flag state
     */
    inline bool excludeSystemFiles() const;

    /**
     * @brief ExcludeSystemFiles flag write accessor
     * @param value the new flag state
     */
    inline void setExcludeSystemFiles(bool value);

    /**
     * @brief InclusionFilter read accessor
     * @return the inclusion filter
     */
    inline const QString& getInclusionFilter() const;

    /**
     * @brief InclusionFilter write accessor
     * @param value the new inclusion filter
     */
    inline void setInclusionFilter(const QString& value);

    /**
     * @brief ExclusionFilter read accessor
     * @return the exclusion filter
     */
    inline const QString& getExclusionFilter() const;

    /**
     * @brief ExclusionFilter write accessor
     * @param value the new exclusion filter
     */
    inline void setExclusionFilter(const QString& value);

private:

    /// Exclude read only files
    bool excludeReadOnly;

    /// Exclude hidden files
    bool excludeHidden;

    /// Exclude system files
    bool excludeSystem;

    /// Include filter
    QString inclusionFilter;

    /// Exclude filter
    QString exclusionFilter;
};

inline bool ComparisonDetails::excludeReadOnlyFiles() const
{
    return excludeReadOnly;
}

inline void ComparisonDetails::setExcludeReadOnlyFiles(bool value)
{
    excludeReadOnly = value;
}

inline bool ComparisonDetails::excludeHiddenFiles() const
{
    return excludeHidden;
}

inline void ComparisonDetails::setExcludeHiddenFiles(bool value)
{
    excludeHidden = value;
}

inline bool ComparisonDetails::excludeSystemFiles() const
{
    return excludeSystem;
}

inline void ComparisonDetails::setExcludeSystemFiles(bool value)
{
    excludeSystem = value;
}

inline const QString& ComparisonDetails::getInclusionFilter() const
{
    return inclusionFilter;
}

inline void ComparisonDetails::setInclusionFilter(const QString& value)
{
    inclusionFilter = value;
}

inline const QString& ComparisonDetails::getExclusionFilter() const
{
    return exclusionFilter;
}

inline void ComparisonDetails::setExclusionFilter(const QString& value)
{
    exclusionFilter = value;
}

/**
 * @brief The ICompare interface defines the common behaviour for
 * all comparison commands. It provide some helpfull methods that
 * is common for all comparison algorithms like comparing the
 * file and directory contents.
 */
class ICompareCommand : public ICommand
{
    Q_OBJECT

public:

    /// Definition of the collection of commands that will be
    /// prepared by the inherited comparison commands.
    typedef QList<ICommand*> CommandCollection;

    /**
     * @brief Creates a new ICompare.
     * @param s The source path.
     * @param t The target path.
     * @param d The comparison details
     * @param b The optional already created batch. If none is provied, a new one will be created.
     * @param parent The parent widget.
     */
    explicit ICompareCommand(const QFileInfo& s, const QFileInfo& t, const ComparisonDetails& d = ComparisonDetails(), BatchCommand* b = 0, QObject *parent = 0);

    /**
     * @brief Finishes the compare command.
     */
    virtual ~ICompareCommand();

    /**
     * @brief The run method is the central point for the ICommand interface.
     * It should trigger, on the inherited classes, the correspondent command
     * activities.
     *
     * @throws The inherited classes should throw an AlgorithmException in
     * case a critical failure prohibits the command execution.
     */
    virtual int run() throw(Error::AlgorithmException);

    /**
     * @brief Enables the user to stop the command execution
     *
     * @throws The inherited classes should throw an AlgorithmException in
     * case a critical failure prohibits the command cancellation.
     */
    virtual void stop() throw(Error::AlgorithmException);

    /**
     * @brief The resultant batch command
     * @returns the list of commands.
     */
    inline BatchCommand* getBatchCommand();

    /**
     * @brief Get the details about the current entry being processed
     * @param entry the QFileInfo details about the entry being processed
     * @param count the amount of entries already discovered
     * @param index the index of the current entry related to the total count
     * @return
     */
    void readCurrentEntryInfo(QFileInfo& entry, int& count, int& index);

private:

    /**
     * @brief Compare the contents of two directories.
     *
     * @param s the source directory
     * @param t the target directory
     * @returns 0 if the directories have the same contents.
     */
    int compareDirectories(const QFileInfo& s, const QFileInfo& t);

    /**
     * @brief Compare the contents of two files.
     *
     * @param s the source file
     * @param t the target file
     * @returns 0 if the files have the same contents.
     */
    int compareFiles(const QFileInfo& s, const QFileInfo& t);

protected:

    /**
     * @brief Add the command to the batch
     * @param cmd the command to be added
     */
    void addCommand(ICommand* cmd);

    /**
     * @brief Method called by the directory comparison methods when
     * the files contents are equal.
     *
     * @param s the source file
     * @param t the target file
     */
    virtual void fileNotChanged(const QFileInfo& s, const QFileInfo& t) = 0;

    /**
     * @brief Method called by the directory comparison methods when
     * the files contents are not equal.
     *
     * @param s the source file
     * @param t the target file
     */
    virtual void fileContentsChanged(const QFileInfo& s, const QFileInfo& t) = 0;

    /**
     * @brief Method called by the directory comparison methods when
     * the files types are different.
     *
     * @param s the source file
     * @param t the target file
     */
    virtual void fileTypeChanged(const QFileInfo& s, const QFileInfo& t) = 0;

    /**
     * @brief Method called by the directory comparison methods when
     * the files last modification timestamps are different.
     *
     * @param s the source file
     * @param t the target file
     */
    virtual void fileLastModificationChanged(const QFileInfo& s, const QFileInfo& t) = 0;

    /**
     * @brief Method called by the directory comparison methods when
     * the file is missing at the source path.
     *
     * @param s the source file
     * @param t the target file
     */
    virtual void fileMissingAtSource(const QFileInfo& s, const QFileInfo& t) = 0;

    /**
     * @brief Method called by the directory comparison methods when
     * the file is missing at the target path.
     *
     * @param s the source file
     * @param t the target file
     */
    virtual void fileMissingAtDestination(const QFileInfo& s, const QFileInfo& t) = 0;

    /**
     * @brief Method called by the directory comparison methods when
     * the directories contents are equal.
     *
     * @param s the source directory
     * @param t the target directory
     */
    virtual void directoryNotChanged(const QFileInfo& s, const QFileInfo& t) = 0;

    /**
     * @brief Method called by the directory comparison methods when
     * the directories contents are not equal.
     *
     * @param s the source directory
     * @param t the target directory
     */
    virtual void directoryContentsChanged(const QFileInfo& s, const QFileInfo& t) = 0;

    /**
     * @brief Method called by the directory comparison methods when
     * the directories types were changed.
     *
     * @param s the source directory
     * @param t the target directory
     */
    virtual void directoryTypeChanged(const QFileInfo& s, const QFileInfo& t) = 0;

    /**
     * @brief Method called by the directory comparison methods when
     * the directory is missing at the source path.
     *
     * @param s the source directory
     * @param t the target directory
     */
    virtual void directoryMissingAtSource(const QFileInfo& s, const QFileInfo& t) = 0;

    /**
     * @brief Method called by the directory comparison methods when
     * the directory is missing at the target path.
     *
     * @param s the source directory
     * @param t the target directory
     */
    virtual void directoryMissingAtDestination(const QFileInfo& s, const QFileInfo& t) = 0;

protected:

    /// Source path to be analyzed
    QFileInfo source;

    /// Target path to be analyzed
    QFileInfo target;

    /// Comparison details
    ComparisonDetails details;

private:

    /// Current Entry being analysed
    QFileInfo entryInfo;

    /// The amount of source folders being analysed
    int entryCount;

    /// The index of the current source folder
    int entryIndex;

    /// Read and write used when readning/writing the current folders
    QReadWriteLock currentSourceFolderLock;

    /// The resultant batch
    BatchCommand* batch;

    /// Flag used to stop the command execution
    volatile bool stopRunning;
};


inline BatchCommand* ICompareCommand::getBatchCommand()
{
    return batch;
}

}
}

#endif // ICOMPARE_H
