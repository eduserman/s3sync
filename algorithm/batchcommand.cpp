/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "batchcommand.h"

namespace S3sync{
namespace Algorithm{

BatchCommand::BatchCommand(QObject *parent) :
    ICommand(parent),
    stopRunning(false)
{
}

BatchCommand::~BatchCommand()
{
    // delete all the commands
    for (CommandCollection::Iterator it = commands.begin(); it != commands.end(); ++it)
        delete *it;

    // clear the list
    commands.clear();
}

int BatchCommand::run() throw(Error::AlgorithmException)
{
    // scan the configured list of commands
    for (CommandCollection::Iterator it = commands.begin(); it != commands.end(); ++it)
    {
        // if the stop command was requested
        if (stopRunning)

            // stop the batch
            break;

        // try to run the command
        (*it)->run();
    }

    /// TODO: we must implement a way to return all the commands results
    return 0;
}

void BatchCommand::stop() throw(Error::AlgorithmException)
{
    stopRunning = true;
}

QString BatchCommand::getDescription() const
{
    return QString(tr("Batch command"));
}

}
}
