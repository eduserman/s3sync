/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "copydircommand.h"
#include <QFile>
#include <QDir>

namespace S3sync{
namespace Algorithm{

CopyDirCommand::CopyDirCommand(const QFileInfo& s, const QFileInfo& t, QObject *parent) :
    IFileSystemCommand(s, t, parent),
    stopRunning(false)
{
}


int CopyDirCommand::run() throw(Error::AlgorithmException)
{
    // make sure we have a valid file to be copied
    if (!source.isDir())
        throw Error::AlgorithmException(QString(tr("CopyDirCommand: Invalid source path (%1)")).arg(source.absoluteFilePath()));
    if (!source.exists())
        throw Error::AlgorithmException(QString(tr("CopyDirCommand: Source directory not found (%1)")).arg(source.absoluteFilePath()));
    if (!target.isDir())
        throw Error::AlgorithmException(QString(tr("CopyDirCommand: Invalid target path (%1)")).arg(target.absoluteFilePath()));
    if (!target.exists())
        throw Error::AlgorithmException(QString(tr("CopyDirCommand: Target directory not found (%1)")).arg(target.absoluteFilePath()));

    // copy it
    return copyDir(QDir(source.absoluteFilePath()),QDir(target.absoluteFilePath())) ? 0 : 1;
}

void CopyDirCommand::stop() throw(Error::AlgorithmException)
{
    // set the flag to true
    stopRunning = true;
}

QString CopyDirCommand::getDescription() const
{
    return QString(tr("Copy (%1) to (%2)"))
            .arg(source.absoluteFilePath())
            .arg(target.absoluteFilePath());
}

bool CopyDirCommand::copyDir(const QDir& s, const QDir& t)
{
    // try to create the target subdirectory
    if (!t.mkdir(s.dirName()))
        return false;

    // send status message
    emit statusMessage(QString(tr("Copying directory %1 to %2")).
                       arg(s.absolutePath()).
                       arg(t.absolutePath()));

    // create a copy pointing to the target dir
    QDir target = t;

    // try to move it to the recently created directory
    if (!target.cd(s.dirName()))
        return false;

    // get the list of files and directories from the current path
    QFileInfoList sourceEntries = s.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot);

    // scan the source file contents
    for(QFileInfoList::Iterator sourceEntryIt = sourceEntries.begin();
        (sourceEntryIt != sourceEntries.end()) && !stopRunning;
        ++sourceEntryIt)
    {
        // get the reference to the current file entry
        QFileInfo sourceEntry = *sourceEntryIt;

        // if the entry on the source directory is a file
        if (sourceEntry.isFile())
        {
            // create the complete file name
            QFileInfo targetEntry = QFileInfo(target,sourceEntry.fileName());

            // send status message
            emit statusMessage(QString(tr("Copying file %1 to %2")).
                               arg(sourceEntry.absoluteFilePath()).
                               arg(targetEntry.absoluteFilePath()));

            // copy the file
            if (!QFile::copy(sourceEntry.absoluteFilePath(),targetEntry.absoluteFilePath()))
                return false;

            // update the target's last modification to the same as the source one
            if (!setLastModified(targetEntry,sourceEntry))
                return false;
        }
        // or if it is a directory
        else if (sourceEntry.isDir())
        {
            // copy the contents
            if (!copyDir(QDir(sourceEntry.absoluteFilePath()),target))
                return false;
        }
    }

    return true;
}

}
}
