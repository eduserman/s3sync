/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "overwritefilecommand.h"

namespace S3sync{
namespace Algorithm{

OverwriteFileCommand::OverwriteFileCommand(const QFileInfo& s, const QFileInfo& t, QObject *parent) :
    IFileSystemCommand(s, t, parent)
{
}

int OverwriteFileCommand::run() throw(Error::AlgorithmException)
{
    // make sure we have a valid file to be copied
    if (!source.isFile())
        throw Error::AlgorithmException(QString(tr("OverwriteFileCommand: Invalid source path (%1)")).arg(source.absoluteFilePath()));
    if (!source.exists())
        throw Error::AlgorithmException(QString(tr("OverwriteFileCommand: Source file not found (%1)")).arg(source.absoluteFilePath()));

    // if the target is an existing file system element
    if (target.exists())
    {
        // and it is a file
        if (target.isFile())
        {
            // delete the target file
            if (!QFile::remove(target.absoluteFilePath()))
            {
                qDebug() << "Error overwriting file " << target.absoluteFilePath();
                return 1;
            }
        }
        // and it is a directory
        else if (target.isDir())
        {
            // update the target to form the complete target name
            target = QFileInfo(QDir(target.absoluteFilePath()),source.fileName());

        }
    }

    // send status message
    emit statusMessage(QString(tr("Overwriting file %1 with %2")).
                       arg(source.absoluteFilePath()).
                       arg(target.absoluteFilePath()));

    // copy it
    return QFile::copy(source.absoluteFilePath(),target.absoluteFilePath()) &&
           setLastModified(source,target) ? 0 : 1;
}

QString OverwriteFileCommand::getDescription() const
{
    return QString(tr("Overwrite (%2) with (%1) contents"))
            .arg(source.absoluteFilePath())
            .arg(target.absoluteFilePath());
}

}
}
