/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef UNIDIRECTIONALCOMPARE_H
#define UNIDIRECTIONALCOMPARE_H

#include "icomparecommand.h"

namespace S3sync{
namespace Algorithm{

/**
 * @brief The UnidirectionalCompare class implements the synchronization
 * algorithm that is meat to mirror the source path contents exactly to
 * the target path. That means: all the changes on the source path are
 * going to be replicated at the target path. It also means that all
 * changes on the target path, that are not present at the source path,
 * are going to be lost.
 */
class UnidirectionalCompare : public ICompareCommand
{
    Q_OBJECT

public:

    /**
     * @brief Creates a new UnidirectionalCompare.
     * @param s The source path.
     * @param t The target path.
     * @param d The comparison details
     * @param b The optional already created batch. If none is provied, a new one will be created.
     * @param parent The parent widget.
     */
    explicit UnidirectionalCompare(const QFileInfo& s, const QFileInfo& t, const ComparisonDetails& details = ComparisonDetails(), BatchCommand* batch = 0, QObject *parent = 0);
    
    /**
     * @brief Read accessor for the command textual description. This
     * will be used by the UI when presenting the commands to the users.
     *
     * @returns the command textual description.
     */
    virtual QString getDescription() const;

signals:
    
public slots:
    
protected:

    /**
     * @brief Method called by the directory comparison methods when
     * the files contents are equal.
     *
     * @param s the source file
     * @param t the target file
     */
    virtual void fileNotChanged(const QFileInfo& s, const QFileInfo& t);

    /**
     * @brief Method called by the directory comparison methods when
     * the files contents are not equal.
     *
     * @param s the source file
     * @param t the target file
     */
    virtual void fileContentsChanged(const QFileInfo& s, const QFileInfo& t);

    /**
     * @brief Method called by the directory comparison methods when
     * the files types are different.
     *
     * @param s the source file
     * @param t the target file
     */
    virtual void fileTypeChanged(const QFileInfo& s, const QFileInfo& t);

    /**
     * @brief Method called by the directory comparison methods when
     * the files last modification timestamps are different.
     *
     * @param s the source file
     * @param t the target file
     */
    virtual void fileLastModificationChanged(const QFileInfo& s, const QFileInfo& t);

    /**
     * @brief Method called by the directory comparison methods when
     * the file is missing at the source path.
     *
     * @param s the source file
     * @param t the target file
     */
    virtual void fileMissingAtSource(const QFileInfo& s, const QFileInfo& t);

    /**
     * @brief Method called by the directory comparison methods when
     * the file is missing at the target path.
     *
     * @param s the source file
     * @param t the target file
     */
    virtual void fileMissingAtDestination(const QFileInfo& s, const QFileInfo& t);

    /**
     * @brief Method called by the directory comparison methods when
     * the directories contents are equal.
     *
     * @param s the source directory
     * @param t the target directory
     */
    virtual void directoryNotChanged(const QFileInfo& s, const QFileInfo& t);

    /**
     * @brief Method called by the directory comparison methods when
     * the directories contents are not equal.
     *
     * @param s the source directory
     * @param t the target directory
     */
    virtual void directoryContentsChanged(const QFileInfo& s, const QFileInfo& t);

    /**
     * @brief Method called by the directory comparison methods when
     * the directories types were changed.
     *
     * @param s the source directory
     * @param t the target directory
     */
    virtual void directoryTypeChanged(const QFileInfo& s, const QFileInfo& t);

    /**
     * @brief Method called by the directory comparison methods when
     * the directory is missing at the source path.
     *
     * @param s the source directory
     * @param t the target directory
     */
    virtual void directoryMissingAtSource(const QFileInfo& s, const QFileInfo& t);

    /**
     * @brief Method called by the directory comparison methods when
     * the directory is missing at the target path.
     *
     * @param s the source directory
     * @param t the target directory
     */
    virtual void directoryMissingAtDestination(const QFileInfo& s, const QFileInfo& t);

private:

    /// Source path to be analyzed
    QFileInfo source;

    /// Target path to be analyzed
    QFileInfo target;
};

}
}

#endif // UNIDIRECTIONALCOMPARE_H
