/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "deletedircommand.h"
#include <QDir>

namespace S3sync{
namespace Algorithm{

DeleteDirCommand::DeleteDirCommand(const QFileInfo& t, QObject *parent) :
    IFileSystemCommand(QFileInfo(), t, parent)
{
}

int DeleteDirCommand::run() throw(Error::AlgorithmException)
{
    // make sure the target directory is valid
    if (!target.isDir())
        throw Error::AlgorithmException(QString(tr("DeleteDirCommand: Invalid target path (%1)")).arg(target.absoluteFilePath()));

    // make sure the target directories exists on the file system
    if (!target.exists())
        throw Error::AlgorithmException(QString(tr("DeleteDirCommand: Target directory not found (%1)")).arg(target.absoluteFilePath()));

    // send status message
    emit statusMessage(QString(tr("Deleting directory %1")).
                       arg(target.absoluteFilePath()));

    // try to remove it
    return QDir(target.absoluteFilePath()).removeRecursively() ? 0 : 1;
}

QString DeleteDirCommand::getDescription() const
{
    return QString(tr("Delete (%1)"))
            .arg(target.absoluteFilePath());
}

}
}
