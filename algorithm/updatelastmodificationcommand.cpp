/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "updatelastmodificationcommand.h"

namespace S3sync{
namespace Algorithm{

UpdateLastModificationCommand::UpdateLastModificationCommand(const QFileInfo& s, const QFileInfo& t, QObject *parent) :
    IFileSystemCommand(s, t, parent)
{
}

int UpdateLastModificationCommand::run() throw(Error::AlgorithmException)
{
    // make sure we have a valid file to be copied
    if (!source.exists())
        throw Error::AlgorithmException(QString(tr("UpdateLastModificationCommand: Source file not found (%1)")).arg(source.absoluteFilePath()));
    if (!target.exists())
        throw Error::AlgorithmException(QString(tr("UpdateLastModificationCommand: Target path not found (%1)")).arg(target.absoluteFilePath()));

    // send status message
    emit statusMessage(QString(tr("Updating Last Modified Date of %1 with %2")).
                       arg(target.absoluteFilePath()).
                       arg(source.absoluteFilePath()));

    // update the target's last modification to the same as the source one
    return setLastModified(source,target) ? 0 : 1;
}

QString UpdateLastModificationCommand::getDescription() const
{
    return QString(tr("Update timestamps of (%1) according to (%2)"))
            .arg(target.absoluteFilePath())
            .arg(source.absoluteFilePath());
}

}
}
