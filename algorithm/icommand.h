/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ICOMMAND_H
#define ICOMMAND_H

#include <QObject>
#include "../error/exception.h"

namespace S3sync{
namespace Algorithm{

/**
 * @brief The ICommand interface defines the common behaviour for
 * all commands to be executed by the synch app. It implements
 * the Command design pattern.
 */
class ICommand : public QObject
{
    Q_OBJECT

public:

    /**
     * @brief Creates a new ICommand.
     * @param parent The parent widget.
     */
    explicit ICommand(QObject *parent = 0);

    /**
     * @brief The run method is the central point for the ICommand interface.
     * It should trigger, on the inherited classes, the correspondent command
     * activities.
     *
     * @throws The inherited classes should throw an AlgorithmException in
     * case a critical failure prohibits the command execution.
     */
    virtual int run() throw(Error::AlgorithmException) = 0;

    /**
     * @brief Enables the user to stop the command execution
     *
     * @throws The inherited classes should throw an AlgorithmException in
     * case a critical failure prohibits the command cancellation.
     */
    virtual void stop() throw(Error::AlgorithmException) = 0;

    /**
     * @brief Read accessor for the command textual description. This
     * will be used by the UI when presenting the commands to the users.
     *
     * @returns the command textual description.
     */
    virtual QString getDescription() const = 0;
    
signals:

    /**
     * @brief statusMessage
     * @param message
     */
    void statusMessage(const QString& message);
    
public slots:
    
};

/**
 * @brief The ICommandRunAdapter is a functional object used to adapt the ICommand::run
 * interface to the QtConcurrent API.
 */
struct ICommandRunAdapter
{
    /// This is necessary to inform the QtConcurrent the type that
    /// will be returned by the method to be executed on the thread pool
    typedef int result_type;

    /**
     * @brief Creates a new ICommandRunAdapter
     * @param cmd the command to be ran.
     */
    ICommandRunAdapter(ICommand* c):cmd(c){Q_ASSERT(cmd);}

    /**
     * @brief operator ()
     */
    int operator()()
    {
        // run the algorithm
        return cmd->run();
    }

    /// The command reference
    ICommand* cmd;
};

/**
 * @brief The ICommandMapAdapter is a functional object used to adapt the ICommand::map
 * interface to the QtConcurrent API.
 */
struct ICommandMapAdapter
{
    /// This is necessary to inform the QtConcurrent the type that
    /// will be returned by the method to be executed on the thread pool
    typedef int result_type;

    /**
     * @brief Creates a new RunAdapter
     * @param cmd the command to be ran.
     */
    ICommandMapAdapter(){}

    /**
     * @brief operator ()
     */
    int operator()(ICommand* cmd)
    {
        // run the algorithm
        return cmd->run();
    }
};

}
}

#endif // ICOMMAND_H
