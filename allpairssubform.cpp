/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "allpairssubform.h"
#include "ui_allpairssubform.h"
#include "connectorsingleton.h"
#include "paireditiondialog.h"
#include "data/iuser.h"

#include <QProgressDialog>
#include <QMessageBox>
#include <QDebug>

AllPairsSubform::AllPairsSubform(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AllPairsSubform)
{
    // call the automatically generated ui configuration method
    ui->setupUi(this);

    // customize the ui automatically generated
    customizeUi();
}

AllPairsSubform::~AllPairsSubform()
{
    delete ui;
}

void AllPairsSubform::updateUi(const S3sync::Model::Pair::Collection& p)
{
    // make a copy of pairs
    pairs = p;

    // update the list of configured pairs
    updatePairsTable();

    // update the enable/disable state according to the ui contents
    updateEnablingState();
}


void AllPairsSubform::on_pushButtonEdit_clicked()
{
    // get the list of selected items
    QList<QTableWidgetItem*> selectedItems =  ui->tableWidget->selectedItems();

    // make sure there is at least one item selected
    if (selectedItems.count() <= 0)
    {
        qDebug() << "The are no selected pairs";
        return;
    }

    // get the row of the first selected item
    int row = (*selectedItems.begin())->row();

    // get the pair id
    int id = ui->tableWidget->item(row,0)->data(Qt::UserRole + ITD_ID).toInt();

    // emit the correspondent signal
    Q_ASSERT(pairs.contains(id));
    emit editPair(pairs[id]);
}

void AllPairsSubform::on_pushButtonDelete_clicked()
{
    // get the list of selected items
    QList<QTableWidgetItem*> selectedItems =  ui->tableWidget->selectedItems();

    // make sure there is at least one item selected
    if (selectedItems.count() <= 0)
    {
        qDebug() << "The are no selected pairs";
        return;
    }

    // get the selected row
    int row = (*selectedItems.begin())->row();

    // get the id
    int id = ui->tableWidget->item(row,TC_ACTIVE)->data(Qt::UserRole + ITD_ID).toInt();

    // emit the correspondent signal
    Q_ASSERT(pairs.contains(id));
    emit deletePair(pairs[id]);
}

void AllPairsSubform::on_pushButtonNew_clicked()
{
    // emit the correspondent signal
    emit createPair();
}

void AllPairsSubform::on_pushButtonPreviewAll_clicked()
{
    // make a private copy of enabled pairs
    S3sync::Model::Pair::Collection activePairs;
    foreach(const S3sync::Model::Pair& pair, pairs)
        if (pair.isActive())
            activePairs.insert(pair.getId(),pair);

    // check if there is at least one active pair
    if (activePairs.size() <= 0)
    {
        QMessageBox::information(this,
                                 tr("No active pairs available."),
                                 tr("There are no active pairs configured. Please activate at least one of them."),
                                 QMessageBox::Ok,
                                 QMessageBox::Ok);
    }
    else
    {
        // emit the correspondent signal
        emit previewPairs(activePairs);
    }
}

void AllPairsSubform::on_pushButtonRunAll_clicked()
{
    // make a private copy of enabled pairs
    S3sync::Model::Pair::Collection activePairs;
    foreach(const S3sync::Model::Pair& pair, pairs)
        if (pair.isActive())
            activePairs.insert(pair.getId(),pair);

    // check if there is at least one active pair
    if (activePairs.size() <= 0)
    {
        QMessageBox::information(this,
                                 tr("No active pairs available"),
                                 tr("There are no active pairs configured. Please activate at least one of them."),
                                 QMessageBox::Ok,
                                 QMessageBox::Ok);
    }
    else
    {
        // emit the correspondent signal
        emit runPairs(activePairs);
    }
}

void AllPairsSubform::on_tableWidget_itemSelectionChanged()
{
    // update the enable/disable state according to the ui contents
    updateEnablingState();
}

void AllPairsSubform::customizeUi()
{
    // preset the column sizes
    ui->tableWidget->setColumnWidth(TC_ACTIVE,60);
    ui->tableWidget->setColumnWidth(TC_NAME,180);
    ui->tableWidget->setColumnWidth(TC_SOURCE_FOLDER,250);
    ui->tableWidget->setColumnWidth(TC_TARGET_FOLDER,250);
}

void AllPairsSubform::updatePairsTable()
{
    // get the amount of pairs to be shown
    int count = pairs.count();

    // preset the row counter
    int row = 0;

    // change the amount of rows
    ui->tableWidget->setRowCount(count);

    // clear the remaining ones
    ui->tableWidget->clearContents();

    // stop sorting
    ui->tableWidget->setSortingEnabled(false);

    // show the modal dialog
    QProgressDialog progress(tr("Updating pairs table..."), tr("Abort"), 0, count, this);
    progress.setWindowModality(Qt::WindowModal);

    // scan the collection
    for (S3sync::Model::Pair::Collection::ConstIterator it = pairs.begin();
         it != pairs.end();
         ++it)
    {
        // update the progress dialog
        progress.setValue(row);

        // stop if cancel was pressed
        if (progress.wasCanceled())
        {
            // update the amount of valid rows and quit
            ui->tableWidget->setRowCount(count);
            break;
        }

        // update the correspondent row...
        updatePairRow(row++,*it);
    }

    // start sorting
    ui->tableWidget->setSortingEnabled(true);
}

void AllPairsSubform::updatePairRow(int row, const S3sync::Model::Pair& pair)
{
    // create the columns
    QTableWidgetItem* activeItem        = new QTableWidgetItem(pair.isActive() ? tr("Yes") : tr("No"));
    QTableWidgetItem* nameItem          = new QTableWidgetItem(pair.getName());
    QTableWidgetItem* sourceFolderItem  = new QTableWidgetItem(pair.getSourcePath());
    QTableWidgetItem* targetFolderItem  = new QTableWidgetItem(pair.getTargetPath());

    // fill up the fields
    ui->tableWidget->setItem(row,TC_ACTIVE, activeItem);
    ui->tableWidget->setItem(row,TC_NAME, nameItem);
    ui->tableWidget->setItem(row,TC_SOURCE_FOLDER,  sourceFolderItem);
    ui->tableWidget->setItem(row,TC_TARGET_FOLDER, targetFolderItem);

    // "secretely" store its id on the item data
    activeItem->setData(Qt::UserRole + ITD_ID,pair.getId());

    // update the alignments
    activeItem->setTextAlignment(Qt::AlignCenter);
}

void AllPairsSubform::updateEnablingState()
{
    // check if we are going to enable or disable the selection dependent buttons
    bool enableOne = (ui->tableWidget->selectedItems().count() != 0);
    bool enableAll = (ui->tableWidget->rowCount() != 0);

    ui->pushButtonDelete->setEnabled(enableOne);
    ui->pushButtonEdit->setEnabled(enableOne);
    ui->pushButtonPreviewAll->setEnabled(enableAll);
    ui->pushButtonRunAll->setEnabled(enableAll);
}


void AllPairsSubform::on_tableWidget_itemDoubleClicked(QTableWidgetItem *item)
{
    // get the row of the first selected item
    int row = item->row();

    // get the pair id
    int id = ui->tableWidget->item(row,0)->data(Qt::UserRole + ITD_ID).toInt();

    // emit the correspondent signal
    Q_ASSERT(pairs.contains(id));
    emit editPair(pairs[id]);
}
