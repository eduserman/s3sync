/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "connectorsingleton.h"
#include "aboutdialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    // start the database interface
    ConnectorSingleton::GetInstance()->start();

    // configure the thread count to one once we want each file
    // to be copied each time in order to avoid network concurrence
    QThreadPool::globalInstance()->setMaxThreadCount(1);

    // star the ui
    ui->setupUi(this);

    // customize the ui
    customizeUi();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent * evt)
{    
    // stop the database interface
    ConnectorSingleton::GetInstance()->stop();

    // continue closing the window...
    QMainWindow::closeEvent(evt);
}

void MainWindow::runPreviewedPairs(const S3sync::Model::Pair::Collection& pairs,S3sync::Algorithm::BatchCommand* cmd)
{
    // update the page synch for this pair
    ui->pageRun->updateUi(pairs,cmd);
    ui->stackedWidget->setCurrentWidget(ui->pageRun);
}

void MainWindow::runPair(const S3sync::Model::Pair& pair)
{
    // create a list of a single pair
    S3sync::Model::Pair::Collection pairs;
    pairs.insert(pair.getId(),pair);

    // update the page synch for this pair
    ui->pageRun->updateUi(pairs);
    ui->stackedWidget->setCurrentWidget(ui->pageRun);
}

void MainWindow::previewPair(const S3sync::Model::Pair& pair)
{
    // create a list of a single pair
    S3sync::Model::Pair::Collection pairs;
    pairs.insert(pair.getId(),pair);

    // update the page synch for this pair
    ui->pagePreview->updateUi(pairs);
    ui->stackedWidget->setCurrentWidget(ui->pagePreview);
}

void MainWindow::runPairs(const S3sync::Model::Pair::Collection& pairs)
{
    // update the page synch for this pair
    ui->pageRun->updateUi(pairs);
    ui->stackedWidget->setCurrentWidget(ui->pageRun);
}

void MainWindow::previewPairs(const S3sync::Model::Pair::Collection& pairs)
{
    // update the page synch for this pair
    ui->pagePreview->updateUi(pairs);
    ui->stackedWidget->setCurrentWidget(ui->pagePreview);
}

void MainWindow::showAllPairs()
{
    // show the all pairs page
    ui->stackedWidget->setCurrentWidget(ui->pageAll);
}

void MainWindow::customizeUi()
{
    QObject::connect( ui->pageAll,SIGNAL(runPair(S3sync::Model::Pair)),
                this,SLOT(runPair(S3sync::Model::Pair)));
    QObject::connect( ui->pageAll,SIGNAL(previewPair(S3sync::Model::Pair)),
                this,SLOT(previewPair(S3sync::Model::Pair)));
    QObject::connect( ui->pageAll,SIGNAL(previewPairs(S3sync::Model::Pair::Collection)),
                this, SLOT(previewPairs(S3sync::Model::Pair::Collection)));
    QObject::connect( ui->pageAll,SIGNAL(runPairs(S3sync::Model::Pair::Collection)),
                this, SLOT(runPairs(S3sync::Model::Pair::Collection)));

    QObject::connect( ui->pagePreview,SIGNAL(closePage()),
                this,SLOT(showAllPairs()));
    QObject::connect( ui->pagePreview,SIGNAL(runPreviewedPairs(S3sync::Model::Pair::Collection,S3sync::Algorithm::BatchCommand*)),
                this,SLOT(runPreviewedPairs(S3sync::Model::Pair::Collection,S3sync::Algorithm::BatchCommand*)));
    QObject::connect( ui->pageRun,SIGNAL(closePage()),
                this,SLOT(showAllPairs()));
}

void MainWindow::on_actionAbout_triggered()
{
    AboutDialog about(this);
    about.exec();
}
