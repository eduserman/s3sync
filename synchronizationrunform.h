/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SYNCHRONIZATIONSUMMARYFORM_H
#define SYNCHRONIZATIONSUMMARYFORM_H

#include <QWidget>
#include <QtConcurrent/QtConcurrent>
#include "model/pair.h"

namespace S3sync{
namespace Algorithm{
class BatchCommand;
class ICompareCommand;
}
}

namespace Ui {
class SynchronizationRunForm;
}

/**
 * @brief The SynchronizationSummaryForm class represents the window
 * that presents the results of the synchronization process.
 */
class SynchronizationRunForm : public QWidget
{
    Q_OBJECT
    
public:

    /**
     * @brief Creates a new SynchronizationSummaryForm
     * @param parent The parent widget
     */
    explicit SynchronizationRunForm(QWidget *parent = 0);

    /**
      * Creates a new SynchronizationSummaryForm
      */
    ~SynchronizationRunForm();
    
    /**
     * @brief Update the ui for running purposes
     * @param pair the pair data
     */
    void updateUi(const S3sync::Model::Pair::Collection& pairs);

    /**
     * @brief Update the ui for running purposes
     * @param pair the pair data
     */
    void updateUi(const S3sync::Model::Pair::Collection& p, S3sync::Algorithm::BatchCommand* cmd);

signals:

    /**
     * @brief Signal sent when the close button is clicked
     */
    void closePage();

private slots:

    /**
     * @brief on_pushButtonClose_clicked
     */
    void on_pushButtonClose_clicked();

    /**
     * @brief previewFinished
     */
    void previewFinished();

    /**
     * @brief runFinished
     */
    void runFinished();

    /**
     * @brief updateStatusTable
     */
    void updateStatusTable(int resultIndex);

    /**
     * @brief updateStatusMessage
     * @param message
     */
    void updateStatusMessage(const QString& message);

private:

    /// Reference to the automatically genereated ui form
    Ui::SynchronizationRunForm *ui;

    /// The pairs being ran
    S3sync::Model::Pair::Collection pairs;

    /// Reference to the comparison algorithm
    S3sync::Algorithm::BatchCommand* previewBatch;

    /// Reference to the file system commands
    S3sync::Algorithm::BatchCommand* runBatch;

    /// The Qt future used for waiting the command result
    QFuture<int> future;

    /// The Qt future watcher used for waiting the command result
    QFutureWatcher<int> watcher;
};

#endif // SYNCHRONIZATIONSUMMARYFORM_H
