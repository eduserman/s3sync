/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef S3SYNC_SQLITE3CONNECTOR_H
#define S3SYNC_SQLITE3CONNECTOR_H

#include <QObject>
#include "iconnector.h"
#include "../error/exception.h"

class QSqlDatabase;

namespace S3sync{
namespace Data{

/**
  * The Sqlite3Connector is the IConnection interface implementation
  * that creates a connection to a Sqlite3 database.
  */
class Sqlite3Connector : public IConnector
{
    Q_OBJECT

public:

    /**
      * Creates a new Sqlite3Connector instance
      * @param parent the parent QObject
      */
    Sqlite3Connector(QObject *parent = 0);

    /**
      * Finishes the Sqlite3Connector
      */
    virtual ~Sqlite3Connector();

    /**
      * @brief Initializes the database interface.
      *
      * Inherited classes must implement this method and make sure
      * that, if no exceptions are thrown, the database is ready
      * to be used.
      */
    virtual void start() throw (Error::DataException);

    /**
      * @brief Finishes the database interface.
      *
      * Inherited classes must implement this method and make sure
      * that all the database resources are released after completion.
      * No exceptions are expected here, therefore the inherited classes
      * must catch and handle all exceptions.
      */
    virtual void stop() throw ();

    /**
      * @brief Get a reference to the IUser interface
      *
      * Inherited classes must implement this interface and return a not
      * null value that points to a IUser implementation.
      */
    virtual IUser* getUserInterface(QObject *parent = 0) throw (Error::DataException);

private:

    /**
      * Get the string containing the path to the database file.
      * @return the database file path.
      */
    QString getDatabaseFilePath();

    /**
      * Check if there is a sqlite3 database file available.
      * @return true if the file was found
      */
    bool isDatabaseSchemaCreated();

    /**
      * Check if database structure matches with the one used by
      * this application release.
      * @return true if the database version is compatible
      */
    bool isDatabaseSchemaCompatible();

    /**
      * Create the database structure from scratch.
      */
    void createDatabaseSchema();

    /**
      * Convert the current database structure to the new one.
      */
    void updateDatabaseSchema();

private:

    /// The private connection to the database
    QSqlDatabase* connection;

private:

    /// File name that will store the database information...
    static const QString SQLITE3_FILE_NAME;
};

}
}

#endif // S3SYNC_SQLITE3CONNECTOR_H
