/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef S3SYNC_SQLITE3CONNECTORFACTORY_H
#define S3SYNC_SQLITE3CONNECTORFACTORY_H

#include "iconnectorfactory.h"

namespace S3sync{
namespace Data{

/**
  * The Sqlite3ConnectorFactory is the IConnectorFactory interface implementation for the Sqlite3 RDBMS
  */
class Sqlite3ConnectorFactory : public IConnectorFactory
{
    Q_OBJECT

public:

    /**
      * Creates a new Sqlite3ConnectorFactory instance
      * @param parent the parent QObject
      */
    explicit Sqlite3ConnectorFactory(QObject *parent = 0);

    /**
      * Finishes the Sqlite3ConnectorFactory
      */
    virtual ~Sqlite3ConnectorFactory();

public:

    virtual IConnector* createConnector(QObject *parent = 0);

signals:

public slots:

};

}
}

#endif // S3SYNC_SQLITE3CONNECTORFACTORY_H
