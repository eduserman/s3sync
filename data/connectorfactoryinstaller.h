/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef S3SYNC_CONNECTORFACTORYINSTALLER_H
#define S3SYNC_CONNECTORFACTORYINSTALLER_H

namespace S3sync{
namespace Data{

class IConnectorFactory;

class ConnectorFactoryInstaller
{
public:
    ConnectorFactoryInstaller(IConnectorFactory* fac);
};

template<typename T>
class ConnectorFactoryInstallerHelper : public ConnectorFactoryInstaller
{
public:
    ConnectorFactoryInstallerHelper():ConnectorFactoryInstaller(&factory){}
private:
    static T factory;
};

template<typename T>
T ConnectorFactoryInstallerHelper<T>::factory;


}
}

#endif // S3SYNC_CONNECTORFACTORYINSTALLER_H
