/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "sqlite3connector.h"
#include "sqlite3user.h"

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>

namespace S3sync{
namespace Data{

const QString Sqlite3Connector::SQLITE3_FILE_NAME = ".s3sync.db.sqlite";

Sqlite3Connector::Sqlite3Connector(QObject *parent) :
    IConnector(parent),
    connection(0)
{
}

Sqlite3Connector::~Sqlite3Connector()
{
    delete connection;
}

void Sqlite3Connector::start() throw (Error::DataException)
{
    if (connection != 0)
    {
        qWarning() << tr("Database connection already initializated");
        return;
    }

    // create a new SQL connection
    QSqlDatabase* conn = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));

    // configure the database
    conn->setDatabaseName(getDatabaseFilePath());

    // try to open the database connection
    if (!conn->open())
    {
        // save the error information
        QSqlError error = conn->lastError();

        // delete the temp connection
        delete conn;

        // throw an exception showing the error cause
        throw Error::DataException(error.databaseText());
    }

    // save the connection for future use
    connection = conn;

    // check if the database schema is available
    if (!isDatabaseSchemaCreated())
    {
        // create the database
        createDatabaseSchema();
    }
    else if (!isDatabaseSchemaCompatible())
    {
        // update the database
        updateDatabaseSchema();
    }
}

void Sqlite3Connector::stop() throw ()
{
    // close if opened
    if (connection)
        connection->close();

    // destroy the connection
    delete connection;
    connection = 0;
}

IUser* Sqlite3Connector::getUserInterface(QObject *parent) throw (Error::DataException)
{
    // if there is no connection alvailable, throw the data exception informing we are
    // not yet ready
    if (0 == connection)
        throw Error::DataException("Database connection not available");

    // return a new instance of the Sqlite3 IUser implementation
    return new Sqlite3User(connection, parent);
}

QString Sqlite3Connector::getDatabaseFilePath()
{
#ifdef Q_OS_UNIX

    // calculate the database file path
    QString path(QDir::home().path());
    path.append(QDir::separator()).append(SQLITE3_FILE_NAME);
    path = QDir::toNativeSeparators(path);

#else

    // calculate the database file path
    QString path(SQLITE3_FILE_NAME);

#endif

    return path;
}

bool Sqlite3Connector::isDatabaseSchemaCreated()
{
    bool ret = false;

    // configure the SQL statement
    QString statement = "SELECT COUNT(name) FROM sqlite_master WHERE type = \"table\" AND (\
        name = \"VERSIONS\")";

    // prepare the sql query looking for the tables we should have there
    QSqlQuery query(*connection);
    query.prepare(statement);

    // execute it
    if ((ret = query.exec()))
    {
        // for all entries
        if ((ret = query.next()))
        {
            // the database is considered created if the necessary amount
            // of tables are there...
            ret = (query.value(0).toInt() == 1);
        }
    }

    return ret;
}

bool Sqlite3Connector::isDatabaseSchemaCompatible()
{
    /// TODO: validate the version table in order to check the version ID.
    return true;
}

void Sqlite3Connector::createDatabaseSchema()
{
    /// TODO: create the first database schema version.
    QSqlQuery query(*connection);

    query.exec("CREATE TABLE PAIRS "
        "(ID INTEGER PRIMARY KEY AUTOINCREMENT, "
        "NAME               VARCHAR(128), "
        "SOURCEPATH         VARCHAR(128), "
        "TARGETPATH         VARCHAR(128), "
        "SYNCHTYPE          INTEGER, "
        "ACTIVE             INTEGER, "
        "EXCLUDEREADONLY    INTEGER, "
        "EXCLUDEHIDDEN      INTEGER, "
        "EXCLUDESYSTEM      INTEGER, "
        "INCLUSIONFILTER    VARCHAR(128) ,"
        "EXCLUSIONFILTER    VARCHAR(128) )");

    query.exec("CREATE TABLE VERSIONS "
        "(MAJOR       INTEGER, "
        " MINOR       INTEGER, "
        " DESCRIPTION VARCHAR(128), "
        " PRIMARY KEY (MAJOR,MINOR) )");

    query.exec("INSERT INTO VERSIONS (MAJOR,MINOR,DESCRIPTION) VALUES (1,0,'VERSION 1.0')");

    query.exec("CREATE TABLE ITEMS "
        "(ID INTEGER PRIMARY KEY AUTOINCREMENT, "
        " PAIR_ID    INTEGER, "
        " SOURCEPATH VARCHAR(128), "
        " SOURCEDATE DATETIME, "
        " TARGETPATH VARCHAR(128), "
        " TARGETDATE DATETIME, "
        " FOREIGN KEY(PAIR_ID) REFERENCES PAIRS(ID) )" );
}

void Sqlite3Connector::updateDatabaseSchema()
{
    /// TODO: based on the current database version, convert the data to the new one.
}

}
}
