/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef S3SYNC_IUSER_H
#define S3SYNC_IUSER_H

#include <QObject>
#include <QMap>

#include "../error/exception.h"
#include "../model/pair.h"

namespace S3sync{
namespace Data{

/**
  * The IUser is the interface that defines the allowed operations
  * on the applications database.
  */
class IUser : public QObject
{
    Q_OBJECT

public:

    /**
      * Creates a new IUser instance
      * @param parent the parent QObject
      */
    explicit IUser(QObject *parent = 0);
    
signals:
    
public slots:
    
public:

    /**
     * @brief Get the list of configured pairs at the database.
     * @param pairList the list of configured pairs.
     */
    virtual bool getPairList(S3sync::Model::Pair::Collection& collection) throw (Error::DataException) = 0;

    /**
     * @brief Get the pair from the database.
     * @param id the pair id
     * @param pair the pair to be read
     */
    virtual bool getPair(S3sync::Model::Pair::Key id, S3sync::Model::Pair& pair) throw (Error::DataException) = 0;

    /**
     * @brief Add a pair do the database.
     * @param pair the pair to be added
     */
    virtual bool addPair(S3sync::Model::Pair& pair) throw (Error::DataException) = 0;

    /**
     * @brief Remove a pair from the database
     * @param pair the pair to be removed
     */
    virtual bool removePair(S3sync::Model::Pair::Key& key) throw (Error::DataException) = 0;

    /**
     * @brief Modify the pair characteristics. The pair to be changed is the one that matches
     * the id of the pair used as parameter.
     * @param pair the pair to be modified.
     */
    virtual bool modifyPair(S3sync::Model::Pair& pair) throw (Error::DataException) = 0;
};

}
}

#endif // S3SYNC_IUSER_H
