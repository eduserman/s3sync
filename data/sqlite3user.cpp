/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "sqlite3user.h"
#include <QSqlQuery>

namespace S3sync{
namespace Data{

Sqlite3User::Sqlite3User(QSqlDatabase* conn, QObject *parent) :
    IUser(parent),
    connection(conn),
    getPairListQuery(new QSqlQuery(*connection)),
    getPairQuery(new QSqlQuery(*connection)),
    addPairQuery(new QSqlQuery(*connection)),
    removePairQuery(new QSqlQuery(*connection)),
    modifyPairQuery(new QSqlQuery(*connection))
{
    getPairListQuery->prepare("SELECT ID, NAME, SOURCEPATH, TARGETPATH, SYNCHTYPE, ACTIVE, EXCLUDEREADONLY, EXCLUDEHIDDEN, EXCLUDESYSTEM, INCLUSIONFILTER, EXCLUSIONFILTER FROM PAIRS");
    getPairQuery    ->prepare("SELECT ID, NAME, SOURCEPATH, TARGETPATH, SYNCHTYPE, ACTIVE, EXCLUDEREADONLY, EXCLUDEHIDDEN, EXCLUDESYSTEM, INCLUSIONFILTER, EXCLUSIONFILTER FROM PAIRS WHERE ID = ?");
    addPairQuery    ->prepare("INSERT INTO PAIRS (NAME, SOURCEPATH, TARGETPATH, SYNCHTYPE, ACTIVE, EXCLUDEREADONLY, EXCLUDEHIDDEN, EXCLUDESYSTEM, INCLUSIONFILTER, EXCLUSIONFILTER) VALUES (?,?,?,?,?,?,?,?,?,?)");
    removePairQuery ->prepare("DELETE FROM PAIRS WHERE ID = ?");
    modifyPairQuery ->prepare("UPDATE PAIRS SET NAME = ?, SOURCEPATH = ?, TARGETPATH = ?, SYNCHTYPE = ?, ACTIVE = ?, EXCLUDEREADONLY = ?, EXCLUDEHIDDEN = ?, EXCLUDESYSTEM = ?, INCLUSIONFILTER = ?, EXCLUSIONFILTER  = ? WHERE ID = ?");
}

Sqlite3User::~Sqlite3User()
{
    delete modifyPairQuery;
    delete removePairQuery;
    delete addPairQuery;
    delete getPairQuery;
    delete getPairListQuery;
}


bool Sqlite3User::getPairList(S3sync::Model::Pair::Collection& collection) throw (Error::DataException)
{
    bool retVal = false;

    // make sure we have a clear list
    collection.clear();

    // run the query
    if ((retVal = getPairListQuery->exec()))
    {
        // for all entries
        while (getPairListQuery->next())
        {
            // get the pair data data
            Model::Pair pair;
            pair.setId(getPairListQuery->value(0).toInt());
            pair.setName(getPairListQuery->value(1).toString());
            pair.setSourcePath(getPairListQuery->value(2).toString());
            pair.setTargetPath(getPairListQuery->value(3).toString());
            pair.setSynchronizationType(static_cast<Model::Pair::SYNC_TYPE>(getPairListQuery->value(4).toInt()));
            pair.setActive(getPairListQuery->value(5).toBool());
            pair.setExcludeReadOnlyFiles(getPairListQuery->value(6).toBool());
            pair.setExcludeHiddenFiles(getPairListQuery->value(7).toBool());
            pair.setExcludeSystemFiles(getPairListQuery->value(8).toBool());
            pair.setInclusionFilter(getPairListQuery->value(9).toString());
            pair.setExclusionFilter(getPairListQuery->value(10).toString());

            // and add it to the output collection
            collection[pair.getId()] = pair;
        }
    }

    return retVal;
}

bool Sqlite3User::getPair(S3sync::Model::Pair::Key key, S3sync::Model::Pair& pair) throw (Error::DataException)
{
    bool retVal = false;

    // configure the statement input parameters
    getPairQuery->bindValue(0, key);

    // run the query
    if ((retVal = getPairQuery->exec()))
    {
        // for that particular entry
        if ((retVal = getPairQuery->next()))
        {
            // get the pair data data
            pair.setId(getPairQuery->value(0).toInt());
            pair.setName(getPairQuery->value(1).toString());
            pair.setSourcePath(getPairQuery->value(2).toString());
            pair.setTargetPath(getPairQuery->value(3).toString());
            pair.setSynchronizationType(static_cast<Model::Pair::SYNC_TYPE>(getPairQuery->value(4).toInt()));
            pair.setActive(getPairQuery->value(5).toBool());
            pair.setExcludeReadOnlyFiles(getPairQuery->value(6).toBool());
            pair.setExcludeHiddenFiles(getPairQuery->value(7).toBool());
            pair.setExcludeSystemFiles(getPairQuery->value(8).toBool());
            pair.setInclusionFilter(getPairQuery->value(9).toString());
            pair.setExclusionFilter(getPairQuery->value(10).toString());
        }
    }

    return retVal;
}

bool Sqlite3User::addPair(S3sync::Model::Pair& pair) throw (Error::DataException)
{
    bool retVal = false;

    // configure the statement input parameters
    addPairQuery->bindValue(0, pair.getName());
    addPairQuery->bindValue(1, pair.getSourcePath());
    addPairQuery->bindValue(2, pair.getTargetPath());
    addPairQuery->bindValue(3, pair.getSynchronizationType());
    addPairQuery->bindValue(4, pair.isActive());
    addPairQuery->bindValue(5, pair.excludeReadOnlyFiles());
    addPairQuery->bindValue(6, pair.excludeHiddenFiles());
    addPairQuery->bindValue(7, pair.excludeSystemFiles());
    addPairQuery->bindValue(8, pair.getInclusionFilter());
    addPairQuery->bindValue(9, pair.getExclusionFilter());

    // run the statement
    if ((retVal = addPairQuery->exec()))
    {
        // update the pair id
        pair.setId(addPairQuery->lastInsertId().toInt());
    }

    return retVal;
}

bool Sqlite3User::removePair(S3sync::Model::Pair::Key& key) throw (Error::DataException)
{
    // configure the statement input parameters
    removePairQuery->bindValue(0, key);

    // run the statement
    return removePairQuery->exec();
}

bool Sqlite3User::modifyPair(S3sync::Model::Pair& pair) throw (Error::DataException)
{
    // configure the statement input parameters
    modifyPairQuery->bindValue(0, pair.getName());
    modifyPairQuery->bindValue(1, pair.getSourcePath());
    modifyPairQuery->bindValue(2, pair.getTargetPath());
    modifyPairQuery->bindValue(3, pair.getSynchronizationType());
    modifyPairQuery->bindValue(4, pair.isActive());
    modifyPairQuery->bindValue(5, pair.excludeReadOnlyFiles());
    modifyPairQuery->bindValue(6, pair.excludeHiddenFiles());
    modifyPairQuery->bindValue(7, pair.excludeSystemFiles());
    modifyPairQuery->bindValue(8, pair.getInclusionFilter());
    modifyPairQuery->bindValue(9, pair.getExclusionFilter());
    modifyPairQuery->bindValue(10, pair.getId());

    // run the statement
    return modifyPairQuery->exec();
}

}
}
