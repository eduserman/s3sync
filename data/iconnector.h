/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef S3SYNC_ICONNECTOR_H
#define S3SYNC_ICONNECTOR_H

#include <QObject>

#include "../error/exception.h"

namespace S3sync{
namespace Data{

class IUser;

/**
  * The IConnector is the interface that enables the access to the
  * database controlling interfaces.
  */
class IConnector : public QObject
{
    Q_OBJECT

public:

    /**
      * Creates a new IConnector instance
      * @param parent the parent QObject
      */
    explicit IConnector(QObject *parent = 0);
    
    /**
      * @brief Initializes the database interface.
      *
      * Inherited classes must implement this method and make sure
      * that, if no exceptions are thrown, the database is ready
      * to be used.
      */
    virtual void start() throw (Error::DataException) = 0;

    /**
      * @brief Finishes the database interface.
      *
      * Inherited classes must implement this method and make sure
      * that all the database resources are released after completion.
      * No exceptions are expected here, therefore the inherited classes
      * must catch and handle all exceptions.
      */
    virtual void stop() throw () = 0;

    /**
      * @brief Get a reference to the IUser interface
      *
      * Inherited classes must implement this interface and return a not
      * null value that points to a IUser implementation.
      */
    virtual IUser* getUserInterface(QObject *parent = 0) throw (Error::DataException) = 0;

signals:
    
public slots:
    
};

}
}

#endif // S3SYNC_ICONNECTOR_H
