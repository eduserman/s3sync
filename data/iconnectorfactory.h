/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef S3SYNC_CONNECTORFACTORY_H
#define S3SYNC_CONNECTORFACTORY_H

#include <QObject>

namespace S3sync{
namespace Data{

class IConnector;

/**
  * The IConnectorFactory is the interface that enables the creation
  * of IConnector instances.
  */
class IConnectorFactory : public QObject
{
    Q_OBJECT

protected:

    /**
      * Creates a new IConnectorFactory instance
      * @param parent the parent QObject
      */
    explicit IConnectorFactory(QObject *parent = 0);

    /**
      * Finishes the IConnectorFactory
      */
    virtual ~IConnectorFactory();

public:

    /**
      * Creates a new IConnector instance.
      * @param the new IConnector instance parent.
      * @return the IConnector created instance
      */
    virtual IConnector* createConnector(QObject *parent = 0) = 0;

    /**
      * Get the reference to the installed IConnectorFactory.
      * @return the default IConnectorFactory.
      * @sa ConnectorFactoryInstaller
      */
    static IConnectorFactory* GetDefault();

signals:

public slots:

private:

    /**
      * Configures the default IConnectorFactory. The method is privated
      * and it is supposed to be called only by the ConnectorFactoryInstaller.
      * @param fac the new default IConnectoryFactory
      * @sa ConnectorFactoryInstaller
      */
    static void SetDefault(IConnectorFactory* fac);

    /// The default IConnectorFactory
    static IConnectorFactory* factory;

    /// Make the ConnectorFactoryInstaller fried in order to allow access to the
    /// SetDefault method.
    friend class ConnectorFactoryInstaller;
};

}
}

#endif // S3SYNC_CONNECTORFACTORY_H
