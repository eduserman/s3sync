/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "connectorsingleton.h"
#include "data/iconnectorfactory.h"
#include "data/iconnector.h"
#include "data/connectorfactoryinstaller.h"
#include "data/sqlite3connectorfactory.h"

// install the sqlite3 as the default database
S3sync::Data::ConnectorFactoryInstallerHelper<S3sync::Data::Sqlite3ConnectorFactory> installer;

ConnectorSingleton::ConnectorSingleton(QObject *parent) :
    QObject(parent),
    user(0),
    connector(0)
{
    // get the reference to the installed factory
    S3sync::Data::IConnectorFactory* factory = S3sync::Data::IConnectorFactory::GetDefault();

    // ask the factory to create the connector (we are passing this as reference to make
    // the QT memory cleanup catch this instance when the singleton dies.
    connector = factory->createConnector(this);
}

ConnectorSingleton* ConnectorSingleton::GetInstance()
{
    static ConnectorSingleton TheInstance;
    return &TheInstance;
}

void ConnectorSingleton::start() throw (S3sync::Error::DataException)
{
    // start the connector
    connector->start();

}

void ConnectorSingleton::stop() throw ()
{
    // start the connector
    connector->stop();
}

S3sync::Data::IUser* ConnectorSingleton::getUser()
{
    // create the user interface when the singleton is accessed for the first time
    if (0 == user)

        // note that the IUser destructor will be called automatically once
        // we are using the singleton as the object parent.
        user = connector->getUserInterface(this);

    return user;
}
