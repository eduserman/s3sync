#include "aboutdialog.h"
#include "ui_aboutdialog.h"

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);

    // if there is a configured parent, centralize the about based on it
    if (parent)
    {
        // get the rectangles that contains the windows geometries
        QRect parentRect = parent->geometry();
        QRect thisRect   = this->geometry();

        // calculate the rectangle that will stay exactly at the
        // parent's center keeping the child's dimension
        QRect newRect((parentRect.width()  - thisRect.width() ) / 2 + parentRect.left(),
                      (parentRect.height() - thisRect.height()) / 2 + parentRect.top(),
                      thisRect.width(),
                      thisRect.height());

        // change the geometry to the centralized position
        this->setGeometry(newRect);
    }
}

AboutDialog::~AboutDialog()
{
    delete ui;
}
