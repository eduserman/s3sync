/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PAIREDITIONDIALOG_H
#define PAIREDITIONDIALOG_H

#include <QDialog>
#include "model/pair.h"

namespace Ui {
class PairEditionDialog;
}

/**
 * @brief The PairEditionDialog class represents the dialog used
 * when creating and editing pair main characteristics.
 */
class PairEditionDialog : public QDialog
{
    Q_OBJECT
    
public:

    /**
     * @brief Creates the new PairEditionDialog
     * @param parent The parent widget
     */
    explicit PairEditionDialog(const S3sync::Model::Pair& pair, QWidget *parent = 0);

    /**
      * Finishes the PairEditionDialog
      */
    ~PairEditionDialog();

    /**
     * @brief Edited pair accessor
     * @return the edit pair
     */
    inline const S3sync::Model::Pair& getPair() const;
    
private slots:

    /**
     * @brief This slot is called when the Browse source folder push button is pressed.
     */
    void on_pushButtonSourceFolder_clicked();

    /**
     * @brief This slot is called when the Browse target folder push button is pressed.
     */
    void on_pushButtonTargetFolder_clicked();

protected:

    /**
     * @brief This method is called when the use pressed the OK button. It will close
     * the ui if all the configured data are acceptable.
     */
    virtual void accept();

private:

    /**
      * This method extracts the information stored on the pair attribute
      * and configures the ui controls.
      */
    void updateControls();

private:

    /// Reference to the automatically genereated ui form
    Ui::PairEditionDialog *ui;

    /// the pair being edited
    S3sync::Model::Pair pair;
};

inline const S3sync::Model::Pair& PairEditionDialog::getPair() const
{
    return pair;
}

#endif // PAIREDITIONDIALOG_H
