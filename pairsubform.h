/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PAIRSUBFORM_H
#define PAIRSUBFORM_H

#include <QWidget>
#include "model/pair.h"

namespace Ui {
class PairSubform;
}

/**
 * @brief The PairSubform class represents the subform loaded on
 * the PairListForm. It shows the details of a particular pair and enables
 * the user to create, edit or delete it. Moreover, it allows the user
 * to start the preview/run synchronization operations.
 */
class PairSubform : public QWidget
{
    Q_OBJECT
    
public:

    /**
     * @brief Creates a new PairSubform
     * @param parent The pared widget
     */
    explicit PairSubform(QWidget *parent = 0);

    /**
      * Finishes the PairSubform
      **/
    ~PairSubform();
    
    /**
      * This method allows its users to request the update of the ui contents.
      */
    void updateUi(const S3sync::Model::Pair& pair);

signals:

    /**
      * Signal that will be sent when a pair should be created
      * @param key the pair identification key
      */
    void createPair();

    /**
      * Signal that will be sent when a pair should be edited
      * @param key the pair identification key
      */
    void editPair(const S3sync::Model::Pair& pair);

    /**
      * Signal that will be sent when a pair is deleted
      * @param key the pair identification key
      */
    void deletePair(const S3sync::Model::Pair& pair);

    /**
      * Signal that is sent when a pair should be ran
      * @param key the pair identification key
      */
    void runPair(const S3sync::Model::Pair& pair);

    /**
      * Signal that is sent when a pair should be previewed
      * @param key the pair identification key
      */
    void previewPair(const S3sync::Model::Pair& pair);

private slots:

    /**
     * @brief This slot is called when the Rename push button is pressed.
     */
    void on_pushButtonEdit_clicked();

    /**
     * @brief This slot is called when the Delete push button is pressed.
     */
    void on_pushButtonDelete_clicked();

    /**
     * @brief This slot is called when the New push button is pressed.
     */
    void on_pushButtonNew_clicked();

    /**
     * @brief This slot is called when the Preview push button is pressed.
     */
    void on_pushButtonPreview_clicked();

    /**
     * @brief This slot is called when the Run push button is pressed.
     */
    void on_pushButtonRun_clicked();

private:

    /// Reference to the automatically genereated ui form
    Ui::PairSubform *ui;

    /// Pair being displayed by the subform
    S3sync::Model::Pair pair;
};

#endif // PAIRSUBFORM_H
