/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "synchronizationrunform.h"
#include "ui_synchronizationrunform.h"
#include "algorithm/batchcommand.h"
#include "algorithm/unidirectionalcompare.h"
#include "algorithm/bidirectionalcompare.h"

SynchronizationRunForm::SynchronizationRunForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SynchronizationRunForm),
    previewBatch(0),
    runBatch(0)
{
    ui->setupUi(this);
}

SynchronizationRunForm::~SynchronizationRunForm()
{
    delete previewBatch;
    delete runBatch;
    delete ui;
}

void SynchronizationRunForm::updateUi(const S3sync::Model::Pair::Collection& p)
{
    Q_ASSERT(p.size()     >  0);
    Q_ASSERT(previewBatch == 0);
    Q_ASSERT(runBatch     == 0);

    // save the pairs data
    pairs = p;

    // clean up both the overview and result tables
    ui->tableWidget->setRowCount(0);

    // update the labels
    if (pairs.size() > 1){
        ui->labelSourceFolder->setText(tr("Multiple"));
        ui->labelTargetFolder->setText(tr("Multiple"));
    }
    else{
        ui->labelSourceFolder->setText(pairs.begin().value().getSourcePath());
        ui->labelTargetFolder->setText(pairs.begin().value().getTargetPath());
    }
    ui->labelStatus->setText(tr("Starting preview..."));

    // create the batch commands used for preview and run
    previewBatch = new S3sync::Algorithm::BatchCommand(this);
    runBatch     = new S3sync::Algorithm::BatchCommand(this);

    // connect the preview batch message status to the ui update routine
    QObject::connect(previewBatch,SIGNAL(statusMessage(QString)),this,SLOT(updateStatusMessage(QString)));

    // for all added pairs
    foreach(S3sync::Model::Pair pair, pairs){

        // read the synchronization details from the pair data
        S3sync::Algorithm::ComparisonDetails details(pair.excludeReadOnlyFiles(), pair.excludeHiddenFiles(), pair.excludeSystemFiles(), pair.getInclusionFilter(),pair.getExclusionFilter());

        // create the comparison command
        S3sync::Algorithm::ICompareCommand* compare = 0;
        if (pair.getSynchronizationType() == S3sync::Model::Pair::ST_UNIDIRECTIONAL)
            compare = new S3sync::Algorithm::UnidirectionalCompare(pair.getSourcePath(), pair.getTargetPath(), details, runBatch);
        else
            compare = new S3sync::Algorithm::BidirectionalCompare(pair.getSourcePath(), pair.getTargetPath(), details, runBatch);

        // add the created compare command to the preview batch
        previewBatch->addCommand(compare);
    }

    // start the asynchronous operation
    future = QtConcurrent::run(S3sync::Algorithm::ICommandRunAdapter(previewBatch));

    // configure the watcher to wait for the future end
    // note that we are connecting the finished signal at this point
    // the reason is that we are disconnecting it everytime the form
    // is closed in order to avoid unexpected previewFinished results.
    watcher.setFuture(future);
    connect(&watcher, SIGNAL(finished()), this, SLOT(previewFinished()));
}

void SynchronizationRunForm::updateUi(const S3sync::Model::Pair::Collection& p, S3sync::Algorithm::BatchCommand* cmd)
{
    // save the pairs data
    pairs = p;

    // clean up both the overview and result tables
    ui->tableWidget->setRowCount(0);

    // update the labels
    if (pairs.size() > 1){
        ui->labelSourceFolder->setText(tr("Multiple"));
        ui->labelTargetFolder->setText(tr("Multiple"));
    }
    else{
        ui->labelSourceFolder->setText(pairs.begin().value().getSourcePath());
        ui->labelTargetFolder->setText(pairs.begin().value().getTargetPath());
    }
    ui->progressBar->setValue(0);

    // update the title text
    ui->labelResultTitle->setText(tr("Synchronizing source and target folders"));

    // update this ui
    ui->labelStatus->setText(tr("Starting synchronization..."));

    // save the reference to the batch command
    runBatch = cmd;

    // connect the batch message status to the ui update routine
    QObject::connect(runBatch,SIGNAL(statusMessage(QString)),this,SLOT(updateStatusMessage(QString)));

    // get the list of commands to be executed
    const S3sync::Algorithm::BatchCommand::CommandCollection& commands = runBatch->getCommands();

    // start the asynchronous operation
    future = QtConcurrent::mapped(commands.begin(),commands.end(),S3sync::Algorithm::ICommandMapAdapter());

    // configure the watcher to wait for the future end
    // note that we are connecting the finished signal at this point
    // the reason is that we are disconnecting it everytime the form
    // is closed in order to avoid unexpected runFinished results.
    watcher.setFuture(future);
    connect(&watcher, SIGNAL(finished()), this, SLOT(runFinished()));
    connect(&watcher, SIGNAL(progressRangeChanged(int,int)), ui->progressBar, SLOT(setRange(int,int)));
    connect(&watcher, SIGNAL(progressValueChanged(int)), ui->progressBar,SLOT(setValue(int)));
    connect(&watcher, SIGNAL(resultReadyAt(int)), this,SLOT(updateStatusTable(int)));
}

void SynchronizationRunForm::on_pushButtonClose_clicked()
{
    // if the future is running
    if (future.isRunning())
    {
        // ask the compare to stop
        if (previewBatch)
        {
            // if it is still running
            previewBatch->stop();
        }
        // ask the run batch to stop
        else if (runBatch)
        {
            // if it is still running
            runBatch->stop();
        }

        // update the label
        ui->labelStatus->setText(tr("Synchronization Stopping..."));

        // wait for the stop
        watcher.waitForFinished();
    }

    // delete any created commands
    delete previewBatch;
    previewBatch = 0;
    delete runBatch;
    runBatch = 0;

    emit closePage();
}

void SynchronizationRunForm::previewFinished()
{
    // update the status message
    ui->labelStatus->setText(tr("Preview complete."));

    // disconnect the watcher
    disconnect(&watcher, SIGNAL(finished()), this, SLOT(previewFinished()));
    future = QFuture<int>();
    watcher.setFuture(future);

    // delete the preview batch
    delete previewBatch;
    previewBatch = 0;

    // update the ui for the batch execution
    updateUi(pairs, runBatch);
}

void SynchronizationRunForm::runFinished()
{
    // disconnect the watcher
    disconnect(&watcher, SIGNAL(finished()), this, SLOT(runFinished()));
    disconnect(&watcher, SIGNAL(progressRangeChanged(int,int)), ui->progressBar, SLOT(setRange(int,int)));
    disconnect(&watcher, SIGNAL(progressValueChanged(int)), ui->progressBar,SLOT(setValue(int)));
    disconnect(&watcher, SIGNAL(resultReadyAt(int)), this,SLOT(updateStatusTable(int)));
    future = QFuture<int>();
    watcher.setFuture(future);

    // update the status bar
    ui->labelStatus->setText(tr("Synchronization Finished"));
    ui->progressBar->setRange(0,1);
    ui->progressBar->setValue(ui->progressBar->maximum());

    // the variables used for sumaryzing
    int totalSuccess = 0;
    int totalError   = 0;
    int totalTotal   = 0;

    // read how many lines we have on the table
    int rowCount = ui->tableWidget->rowCount();

    // scan those lines
    for (int i = 0; i < rowCount; ++i)
    {
        // read those items
        QTableWidgetItem* itemSuccess = ui->tableWidget->item(i, 1);
        QTableWidgetItem* itemError   = ui->tableWidget->item(i, 2);
        QTableWidgetItem* itemTotal   = ui->tableWidget->item(i, 3);

        // and add them to the totals
        totalSuccess += itemSuccess->text().toInt();
        totalError   += itemError->text().toInt();
        totalTotal   += itemTotal->text().toInt();
    }

    // Add the summary lines
    ui->tableWidget->setRowCount(rowCount + 1);
    ui->tableWidget->setItem(rowCount , 0, new QTableWidgetItem(tr("All Operations")));
    ui->tableWidget->setItem(rowCount , 1, new QTableWidgetItem(QString("%1").arg(totalSuccess)));
    ui->tableWidget->setItem(rowCount , 2, new QTableWidgetItem(QString("%1").arg(totalError)));
    ui->tableWidget->setItem(rowCount , 3, new QTableWidgetItem(QString("%1").arg(totalTotal)));

    // change the fonts to bold
    QFont font = ui->tableWidget->item(rowCount,0)->font();
    font.setBold(true);
    ui->tableWidget->item(rowCount,0)->setFont(font);
    ui->tableWidget->item(rowCount,1)->setFont(font);
    ui->tableWidget->item(rowCount,2)->setFont(font);
    ui->tableWidget->item(rowCount,3)->setFont(font);

    // update the title text
    if (rowCount <= 0)
        ui->labelResultTitle->setText(tr("The source and target folders are already synchronized"));
    else if (totalError <= 0)
        ui->labelResultTitle->setText(tr("The synchronization ran without errors"));
    else
        ui->labelResultTitle->setText(tr("There were errors when synchronizing source and target folders"));

    // delete the run batch
    delete runBatch;
    runBatch = 0;
}

void SynchronizationRunForm::updateStatusTable(int resultIndex)
{
    QString commandName = tr("Undefined Command Name");

    // get the list of commands to be executed
    const S3sync::Algorithm::BatchCommand::CommandCollection& commands = runBatch->getCommands();

    // if there are no commands to be executed, just leave
    if (commands.empty())
        return;

    // get the reference to the command where the result is now available
    const S3sync::Algorithm::ICommand* cmd = commands.value(resultIndex);

    // configure the command name according to the type
    if (cmd->inherits("S3sync::Algorithm::CopyDirCommand"))
    {
        commandName = tr("Copy Directory");
    }
    else if (cmd->inherits("S3sync::Algorithm::CopyFileCommand"))
    {
        commandName = tr("Copy File");
    }
    else if (cmd->inherits("S3sync::Algorithm::OverwriteFileCommand"))
    {
        commandName = tr("Overwrite File");
    }
    else if (cmd->inherits("S3sync::Algorithm::DeleteDirCommand"))
    {
        commandName = tr("Delete Directory");
    }
    else if (cmd->inherits("S3sync::Algorithm::DeleteFileCommand"))
    {
        commandName = tr("Delete File");
    }
    else if (cmd->inherits("S3sync::Algorithm::UpdateLastModificationCommand"))
    {
        commandName = tr("Update Modification Timestamp");
    }

    // the amount of success and error on this particular type of operation
    int currentSuccess = 0;
    int currentError  = 0;

    // these are the table widget items that represent this particular type of command
    QTableWidgetItem* itemName    = 0;
    QTableWidgetItem* itemSuccess = 0;
    QTableWidgetItem* itemError   = 0;
    QTableWidgetItem* itemTotal   = 0;

    // look for that type of command on the results table
    QList<QTableWidgetItem*> commandItems = ui->tableWidget->findItems(commandName,Qt::MatchExactly);

    // if that command was not added already to the table, do it now
    if (commandItems.empty())
    {
        // current count of rows
        int rowCount = ui->tableWidget->rowCount();

        // add additional space for the new command
        ui->tableWidget->setRowCount(rowCount + 1);

        // create the items
        itemName    = new QTableWidgetItem(commandName);
        itemSuccess = new QTableWidgetItem("0");
        itemError   = new QTableWidgetItem("0");
        itemTotal   = new QTableWidgetItem("0");

        // add the new command at the end of the tabl
        ui->tableWidget->setItem(rowCount , 0, itemName);
        ui->tableWidget->setItem(rowCount , 1, itemSuccess);
        ui->tableWidget->setItem(rowCount , 2, itemError);
        ui->tableWidget->setItem(rowCount , 3, itemTotal);
    }
    else
    {
        // load the existing items
        itemName    = commandItems.first();
        itemSuccess = ui->tableWidget->item(itemName->row(), 1);
        itemError   = ui->tableWidget->item(itemName->row(), 2);
        itemTotal   = ui->tableWidget->item(itemName->row(), 3);
        Q_ASSERT(itemSuccess && itemError && itemTotal);

        // read the current amount of successes and errors
        currentSuccess = itemSuccess->text().toInt();
        currentError   = itemError->text().toInt();
    }

    // update the counters
    if (watcher.future().resultAt(resultIndex) == 0)
        currentSuccess++;
    else
        currentError++;

    // update the table
    itemSuccess->setText(QString("%1").arg(currentSuccess));
    itemError  ->setText(QString("%1").arg(currentError));
    itemTotal  ->setText(QString("%1").arg(currentSuccess + currentError));
}

void SynchronizationRunForm::updateStatusMessage(const QString& message)
{
    // update the status bar
    ui->labelStatus->setText(message);
}
