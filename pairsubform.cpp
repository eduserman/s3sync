/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "pairsubform.h"
#include "ui_pairsubform.h"

PairSubform::PairSubform(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PairSubform)
{
    ui->setupUi(this);
}

PairSubform::~PairSubform()
{
    delete ui;
}

void PairSubform::updateUi(const S3sync::Model::Pair& p)
{
    QString synchronizationType;

    // copy the pair information for further use
    pair = p;

    // according to the pair's synchronization type
    // configure the string accordingly.
    switch(p.getSynchronizationType())
    {
    case S3sync::Model::Pair::ST_UNIDIRECTIONAL:
        synchronizationType = tr("Unidirectional");
    break;
    case S3sync::Model::Pair::ST_BIDIRECTIONAL:
        synchronizationType = tr("Bidirectional");
    break;
    default:
        synchronizationType = tr("Invalid Synchronization Type");
    break;
    }

    // update the ui
    ui->labelPairName->setText(p.getName());
    ui->labelSourceFolder->setText(p.getSourcePath());
    ui->labelTargetFolder->setText(p.getTargetPath());
    ui->labelSynchMode->setText(synchronizationType);
}

void PairSubform::on_pushButtonEdit_clicked()
{
    // emit the correspondent signal
    emit editPair(pair);
}

void PairSubform::on_pushButtonDelete_clicked()
{
    // emit the correspondent signal
    emit deletePair(pair);
}

void PairSubform::on_pushButtonNew_clicked()
{
    // emit the correspondent signal
    emit createPair();
}

void PairSubform::on_pushButtonPreview_clicked()
{
    // emit the correspondent signal
    emit previewPair(pair);
}

void PairSubform::on_pushButtonRun_clicked()
{
    // emit the correspondent signal
    emit runPair(pair);
}
