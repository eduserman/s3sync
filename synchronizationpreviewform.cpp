/*
 * S3 Synchronization Tool
 * Copyright (C) 2013 Eduardo Jonson Serman
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "synchronizationpreviewform.h"
#include "ui_synchronizationpreviewform.h"
#include "algorithm/batchcommand.h"
#include "algorithm/ifilesystemcommand.h"
#include "algorithm/unidirectionalcompare.h"
#include "algorithm/bidirectionalcompare.h"

SynchronizationPreviewForm::SynchronizationPreviewForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SynchronizationPreviewForm),
    previewBatch(0),
    runBatch(0)
{
    ui->setupUi(this);

    // preconfigure the table view columns width
    ui->tableWidget->setColumnWidth(CN_ACTIVE,50);
    ui->tableWidget->setColumnWidth(CN_OPERATION,100);
    ui->tableWidget->setColumnWidth(CN_SOURCE_PATH,205);
    ui->tableWidget->setColumnWidth(CN_SOURCE_FILE,100);
    ui->tableWidget->setColumnWidth(CN_SOURCE_LAST,120);
    ui->tableWidget->setColumnWidth(CN_TARGET_PATH,205);
    ui->tableWidget->setColumnWidth(CN_TARGET_FILE,100);
    ui->tableWidget->setColumnWidth(CN_TARGET_LAST,120);
}

SynchronizationPreviewForm::~SynchronizationPreviewForm()
{
    delete previewBatch;
    delete runBatch;
    delete ui;
}

void SynchronizationPreviewForm::updateUi(const S3sync::Model::Pair::Collection& p)
{
    Q_ASSERT(p.size()     >  0);
    Q_ASSERT(previewBatch == 0);
    Q_ASSERT(runBatch     == 0);

    // save the pairs data
    pairs = p;

    // clean up both the overview and result tables
    ui->tableWidget->setRowCount(0);
    ui->tableWidgetResults->setRowCount(0);

    // update the buttons enabling state
    ui->pushButtonStop->setEnabled(true);
    ui->pushButtonRun->setEnabled(false);

    // update the labels
    if (pairs.size() > 1){
        ui->labelSourceFolder->setText(tr("Multiple"));
        ui->labelTargetFolder->setText(tr("Multiple"));
    }
    else{
        ui->labelSourceFolder->setText(pairs.begin().value().getSourcePath());
        ui->labelTargetFolder->setText(pairs.begin().value().getTargetPath());
    }
    ui->labelStatus->setText(tr("Starting preview..."));

    // create the batch commands used for preview and run
    previewBatch = new S3sync::Algorithm::BatchCommand(this);
    runBatch     = new S3sync::Algorithm::BatchCommand(this);

    // connect the preview batch message status to the ui update routine
    QObject::connect(previewBatch,SIGNAL(statusMessage(QString)),this,SLOT(updateStatusMessage(QString)));

    // for all added pairs
    foreach(S3sync::Model::Pair pair, pairs){

        // read the synchronization details from the pair data
        S3sync::Algorithm::ComparisonDetails details(pair.excludeReadOnlyFiles(), pair.excludeHiddenFiles(), pair.excludeSystemFiles(), pair.getInclusionFilter(),pair.getExclusionFilter());

        // create the comparison command
        S3sync::Algorithm::ICompareCommand* compare = 0;
        if (pair.getSynchronizationType() == S3sync::Model::Pair::ST_UNIDIRECTIONAL)
            compare = new S3sync::Algorithm::UnidirectionalCompare(pair.getSourcePath(), pair.getTargetPath(), details, runBatch);
        else
            compare = new S3sync::Algorithm::BidirectionalCompare(pair.getSourcePath(), pair.getTargetPath(), details, runBatch);

        // add the created compare command to the preview batch
        previewBatch->addCommand(compare);
    }

    // start the asynchronous operation
    future = QtConcurrent::run(S3sync::Algorithm::ICommandRunAdapter(previewBatch));

    // configure the watcher to wait for the future end
    // note that we are connecting the finished signal at this point
    // the reason is that we are disconnecting it everytime the form
    // is closed in order to avoid unexpected previewFinished results.
    watcher.setFuture(future);
    connect(&watcher, SIGNAL(finished()), this, SLOT(previewFinished()));
}

void SynchronizationPreviewForm::on_pushButtonRun_clicked()
{
    Q_ASSERT(runBatch != 0);

    // scan all the rows on the table
    for (int i = 0; i < ui->tableWidget->rowCount(); i++)
    {
        // read the item at the active position
        QTableWidgetItem *active = ui->tableWidget->item(i,CN_ACTIVE);

        // commands that are still checked will not be removed from the batch
        if (active->checkState() == Qt::Checked)
            continue;

        // read the data stored at it
        void* activeData = active->data(Qt::UserRole).value<void*>();

        // cast it to the IFileSystemCommand once this was the type used when storing it
        S3sync::Algorithm::IFileSystemCommand* activeDataCommand = reinterpret_cast<S3sync::Algorithm::IFileSystemCommand*>(activeData);

        // it will be deleted later by the app main loop
        activeDataCommand->deleteLater();

        // remove it from the batch command
        runBatch->removeCommand(activeDataCommand);
    }

    // run the batch command
    emit runPreviewedPairs(pairs,runBatch);

    // release the reference to the run batch because someone else
    // holpefully will release it...
    runBatch = 0;
}

void SynchronizationPreviewForm::on_pushButtonStop_clicked()
{
    // if the command is running
    if (future.isRunning() && previewBatch)
    {
        // ask the compare to stop
        if (previewBatch)
            previewBatch->stop();

        // and wait for it. it is important to remember that
        // the preview finished will be triggered and the ui
        // will be updated accordingly
        watcher.waitForFinished();
    }
}

void SynchronizationPreviewForm::on_pushButtonClose_clicked()
{
    // disconnect the watcher in order to avoid receiving
    // unexpected previewFinished calls.
    disconnect(&watcher, SIGNAL(finished()), this, SLOT(previewFinished()));

    // simulate the user clicking the stop button
    on_pushButtonStop_clicked();

    // delete any created commands
    delete previewBatch;
    previewBatch = 0;
    delete runBatch;
    runBatch = 0;

    // and close the preview page
    emit closePage();
}

void SynchronizationPreviewForm::previewFinished()
{
    int currentRow = 0;

    // map used to sum the results of the preview
    typedef QMap<QString,qint32> ResultsMap;
    ResultsMap results;

    // stop the update timer and update the status message
    ui->labelStatus->setText(tr("Preview complete."));

    // get the list of resulting commands
    const S3sync::Algorithm::ICompareCommand::CommandCollection& commands = runBatch->getCommands();

    // configure the amount of rows on the table accordingly
    ui->tableWidget->setRowCount(commands.count());

    // scan all the command list and configure the ui accordingly
    for (S3sync::Algorithm::ICompareCommand::CommandCollection::ConstIterator it = commands.begin(); it != commands.end(); ++it)
    {
        QString commandName = tr("Undefined Command Name");
        QString sourcePath  = tr("");
        QString sourceFile  = tr("");
        QString sourceLast  = tr("");
        QString targetPath  = tr("");
        QString targetFile  = tr("");
        QString targetLast  = tr("");

        // get the reference to the current command. not that
        // we are expecting file system commands at this point
        const S3sync::Algorithm::IFileSystemCommand* cmd = qobject_cast<S3sync::Algorithm::IFileSystemCommand*>(*it);

        // if this is not a file system commands, we are going
        // to log the information and keep going
        if (cmd == 0)
        {
            qDebug() << "Unexcpected command " << (*it)->metaObject()->className();
            continue;
        }

        // configure the command name according to the type
        if (cmd->inherits("S3sync::Algorithm::CopyDirCommand"))
        {
            commandName = tr("Copy Directory");
            sourcePath  = cmd->getSource().absoluteFilePath();
            sourceFile  = tr("");
            sourceLast  = cmd->getSource().lastModified().toString(Qt::LocalDate);
            targetPath  = cmd->getTarget().absoluteFilePath();
            targetFile  = tr("");
            targetLast  = cmd->getTarget().lastModified().toString(Qt::LocalDate);
        }
        else if (cmd->inherits("S3sync::Algorithm::CopyFileCommand"))
        {
            commandName = tr("Copy File");
            sourcePath  = cmd->getSource().absolutePath();
            sourceFile  = cmd->getSource().fileName();
            sourceLast  = cmd->getSource().lastModified().toString(Qt::LocalDate);
            targetPath  = cmd->getTarget().absoluteFilePath();
            targetFile  = tr("");
            targetLast  = cmd->getTarget().lastModified().toString(Qt::LocalDate);
        }
        else if (cmd->inherits("S3sync::Algorithm::OverwriteFileCommand"))
        {
            commandName = tr("Overwrite File");
            sourcePath  = cmd->getSource().absolutePath();
            sourceFile  = cmd->getSource().fileName();
            sourceLast  = cmd->getSource().lastModified().toString(Qt::LocalDate);
            targetPath  = cmd->getTarget().absolutePath();
            targetFile  = cmd->getTarget().fileName();
            targetLast  = cmd->getTarget().lastModified().toString(Qt::LocalDate);
        }
        else if (cmd->inherits("S3sync::Algorithm::DeleteDirCommand"))
        {
            commandName = tr("Delete Directory");
            sourcePath  = tr("");
            sourceFile  = tr("");
            targetPath  = cmd->getTarget().absoluteFilePath();
            targetFile  = tr("");
            targetLast  = cmd->getTarget().lastModified().toString(Qt::LocalDate);
        }
        else if (cmd->inherits("S3sync::Algorithm::DeleteFileCommand"))
        {
            commandName = tr("Delete File");
            sourcePath  = tr("");
            sourceFile  = tr("");
            targetPath  = cmd->getTarget().absolutePath();
            targetFile  = cmd->getTarget().fileName();
            targetLast  = cmd->getTarget().lastModified().toString(Qt::LocalDate);
        }
        else if (cmd->inherits("S3sync::Algorithm::UpdateLastModificationCommand"))
        {
            commandName = tr("Update Modification Timestamp");
            sourcePath  = cmd->getSource().absolutePath();
            sourceFile  = cmd->getSource().fileName();
            sourceLast  = cmd->getSource().lastModified().toString(Qt::LocalDate);
            targetPath  = cmd->getTarget().absolutePath();
            targetFile  = cmd->getTarget().fileName();
            targetLast  = cmd->getTarget().lastModified().toString(Qt::LocalDate);
        }

        {
            // look for that operation on the results map
            ResultsMap::Iterator it = results.find(commandName);

            // if not already there
            if (it == results.end())
                // add it to the map
                results[commandName] = 1;
            else
            {
                // increment the counter
                qint32 currentCount = *it;
                *it = ++currentCount;
            }
        }

        // the first column on the table has some special features. it
        // might be checked or unchecked by the user. additionally it
        // will store the correspondent command later on
        QTableWidgetItem* active = new QTableWidgetItem();
        active->setCheckState(Qt::Checked);
        active->setData(Qt::UserRole,QVariant::fromValue((void*)cmd));

        // configure the table item accodingly
        ui->tableWidget->setItem(currentRow,CN_ACTIVE,active);
        ui->tableWidget->setItem(currentRow,CN_OPERATION,new QTableWidgetItem(commandName));
        ui->tableWidget->setItem(currentRow,CN_SOURCE_PATH,new QTableWidgetItem(sourcePath));
        ui->tableWidget->setItem(currentRow,CN_SOURCE_FILE,new QTableWidgetItem(sourceFile));
        ui->tableWidget->setItem(currentRow,CN_SOURCE_LAST,new QTableWidgetItem(sourceLast));
        ui->tableWidget->setItem(currentRow,CN_TARGET_PATH,new QTableWidgetItem(targetPath));
        ui->tableWidget->setItem(currentRow,CN_TARGET_FILE,new QTableWidgetItem(targetFile));
        ui->tableWidget->setItem(currentRow,CN_TARGET_LAST,new QTableWidgetItem(targetLast));

        // update the row index
        currentRow++;
    }

    // reset the row counter because we will need it when configuring the results table view
    currentRow = 0;

    // get the list of keys that will correspond to the operation types to be executed
    QList<QString> keys = results.keys();

    // configure the amount of row according to the operations to be executed
    ui->tableWidgetResults->setRowCount(keys.count());

    // scan the found operations
    for(QList<QString>::Iterator it = keys.begin(); it != keys.end(); ++it)
    {
        // get the operation name
        QString key = *it;

        // update the correspondent rows
        ui->tableWidgetResults->setItem(currentRow ,0,new QTableWidgetItem(key));
        ui->tableWidgetResults->setItem(currentRow ,1,new QTableWidgetItem(QString("%1").arg(results[key])));

        // update the row index
        currentRow++;
    }

    // update the buttons enabling state
    ui->pushButtonStop->setEnabled(false);
    ui->pushButtonRun->setEnabled(currentRow != 0);

    // delete any created preview commands
    delete previewBatch;
    previewBatch = 0;
}

void SynchronizationPreviewForm::updateStatusMessage(const QString& message)
{
    // update the status bar
    ui->labelStatus->setText(message);
}
